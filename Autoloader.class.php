<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
/*
 * @todo remove hard coded link to template.
 * @todo display "backup" html page with error message,
 * depending on user role.
 */
namespace Library;

use \Exception;
use \Library\Errors\ErrorException;

class Autoloader
{
    public function __construct()
    {
        $this->initErrorHandlers();
        require_once 'External/PHPMailer/PHPMailerAutoload.php';
        spl_autoload_register(array($this, 'loader'), true, false);
    }

    public function errorToException($code, $message, $file, $line)
    {
        throw new ErrorException($message, 0, $code, $file, $line);
    }

    /*
    * Never throw exception in my exception handler (here customException).
    * It would create an infinite loop, which would generate a fatal error:
    * "Exception thrown without a stack frame in Unknown on line 0".
    *
    * Sets the default exception handler if an exception is not caught within a try/catch block.
    * Execution will stop after the exception_handler is called.
    * http://nl3.php.net/set_exception_handler
    */
    public function customException($e)
    {
        echo "<pre style=\"font-size:16px;\">{$e->__tostring()}".PHP_EOL."{$e->getTraceAsString()}</pre>";
    }

    private function initErrorHandlers()
    {
        /*
        * Remember to use the namespace even for the functions outside objects.
        * If PHP does not find the function, it will issue a warning for invalid callback.
        */
        set_error_handler(array($this, 'errorToException'));
        set_exception_handler(array($this, 'customException'));
    }

    private function loader($class)
    {
        $isFileLoaded = false;
        $root = '../';
        $logsPath = $root.'Logs';
        chdir(dirname(__FILE__));
        $message = '';
        $className = $class;
        $fileExtension = '.';
        if (($lastSeparatorPosition = strrpos($class, '\\')) !== false) {
            $className = substr($class, $lastSeparatorPosition + 1);
        }
        if (!preg_match('/^I[A-Z]/', $className)) {
            $fileExtension .= 'class.';
        }
        $fileExtension .= 'php';
        $file = $root.str_replace('\\', '/', $class).$fileExtension;
        if (file_exists($file)) {
            include_once $file;
            $isFileLoaded = true;
        } else {
            $message = "File $file does not exist.";
        }
        if (!$isFileLoaded) {
            $errorManager = new \Library\ErrorManager($logsPath);
            $errorManager->logError(__METHOD__.':'.__LINE__.": Could not load $class: $message.");
        }
        /**
         * PHP autoloaders are supposed to silently return false if a class cannot be loaded.
         * This is required to allow chaining multiple autoloaders.
         * @source http://stackoverflow.com/a/23653422
         */
        return $isFileLoaded;
    }
}
