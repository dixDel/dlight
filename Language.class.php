<?php
namespace Library;

use Library\Entity;

class Language extends Entity
{
    private $isActive = true;

    public function isValid()
    {
        return true;
    }

    /**
     * @return isActive
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param $isActive
     * @return self
     */
    public function setIsActive()
    {
        $this->isActive = true;
        return $this;
    }

    public function setIsNotActive()
    {
        $this->isActive = false;
        return $this;
    }
}
