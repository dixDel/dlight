<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Errors;

/**
 * Description of ErrorException
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class ErrorException extends \ErrorException
{
    public function __toString()
    {
        switch($this->severity)
        {
            case E_USER_ERROR:
                $type = 'Fatal error';
                break;
            case E_WARNING:
            case E_USER_WARNING:
                $type = 'Warning';
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                $type = 'Notice';
                break;
            default:
                $type = 'Unknown error';
                break;
        }
        return "[$type (code $this->code)] catched by my custom handler in \"".
            $this->file."\" at line $this->line: $this->message";
    }
}
