<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\FileManagers;

use \Library\FileManager;

/**
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class CsvFileManager extends FileManager
{
    private $enclosure = '"';
    private $headersRow = array();
    private $separateFirstRow = false;

    protected function parseNextLineAsArray()
    {
        return fgetcsv($this->handle(), 0, ',', $this->enclosure);
    }

    protected function importData()
    {
        $csvLineAsArray = array();
        if ($this->openFile()) {
            $isFirstLoop = true;
            while (($csvLineAsArray = $this->parseNextLineAsArray()) !== false) {
                if ($isFirstLoop && $this->separateFirstRow) {
                    $this->headersRow = str_replace('"', '', $csvLineAsArray);
                } else {
                    $this->setRawData(
                        array_combine($this->entity()->attributeNames(), str_replace('"', '', $csvLineAsArray))
                    );
                    $this->castAndStoreEntity();
                }
                $isFirstLoop = false;
            }
        }
        $this->closeFile();
    }

    /**
     * @param $enclosure
     * @return self
     */
    public function setEnclosure($enclosure)
    {
        $this->enclosure = $enclosure;
        return $this;
    }

    /**
     * @return self
     */
    public function setSeparateFirstRow()
    {
        $this->separateFirstRow = true;
        return $this;
    }

    /**
     * @return headersRow
     */
    public function headersRow()
    {
        return $this->headersRow;
    }
}
