<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Library\ApplicationComponent;

/**
 * Description of HTTPResponse
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class HTTPResponse extends ApplicationComponent
{
    private $status = 1;

    public function addHeader($header)
    {
        header($header);
    }

    public function redirect($location)
    {
        $this->addHeader('Location: '.$location);
        $this->send(true);
    }

    public function redirectReferer()
    {
        $location = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';
        $this->redirect($location);
    }

    public function redirect403(Page $page)
    {
        $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__);
        $this->app->page()->setContentFile($this->app->config()->getString('errorsPath').'/403.php');
        $this->status = 403;
        $this->addHeader('HTTP/1.0 403 Forbidden');
        $this->send();
    }

    public function redirect404(Page $page, $message = '')
    {
        //$this->app->page() = new Page($this->app);
        $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__);
        $this->app->page()->setContentFile($this->app->config()->getString('errorsPath').'/404.php');
        $this->status = 404;
        $this->app->page()->addVar('message', $message);
        $this->addHeader('HTTP/1.0 404 Not Found');
        $this->send();
    }

    public function send($redirect = false)
    {
        //$this->app->errorManager()->logTime();
        //$this->app->errorManager()->saveLog();
        if ($this->app->page()) {
            $this->app->page()->addVar('status', $this->status);
        }
        if ($redirect) {
            exit;
        } else {
            exit($this->app->page()->getGeneratedPage());
        }
    }

    // The default value for httpOnly is false, it is more secure to set it to true
    public function setCookie(
        $name,
        $value = '',
        $expire = 0,
        $path = null,
        $domain = null,
        $secure = false,
        $httpOnly = true
    ) {
        setcookie($name, $value, $expire, $path, $domain, $secure, $httpOnly);
    }
}
