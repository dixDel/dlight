<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \DateTime;
use \Exception;
use \Library\Config;
use \Library\Entity;
use \Library\Manager;
use \PDO;
use \PDOException;
use \RuntimeException;

/**
 * Description of Manager
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class DatabaseManager extends Manager
{
    private $api = '';
    private $driver = '';
    private $host = '';
    private $username = '';
    private $password = '';
    private $tupleName = 'tupleNameNotSet';
    private $tupleAlias = 'tupleAliasNotSet';
    private $statement = null;
    // Store a copy of Entity's attributes to use in insert/update queries.
    private $attributeNamesForSaving = array();
    // Store the final string generated from self::$attributeNamesForSaving.
    private $attributesString = '';
    private $joinType = '';
    private $joins = array();
    private $joinId = 0; // keep track of join statements and their conditions if any
    private $joinConditions = array();
    private $groupByAttributes = array();
    private $havingCount = 0;
    private $orderBy = array();
    private $limitOffset = 0;
    private $limitCount = 0;
    private $anonymousValuesStrings = array();
    private $valuesToBind = array();
    private $valuesToBindCount = 1;
    private $lastInsertId = 0;
    private $selectConditions = array();
    private $updateAttribute = 'id';
    private $deleteAttribute = 'id';
    private $relationAttributeName = '';
    private $debug = false;
    protected $databaseName = '';
    /**
     * Database Access Object
     * @var mixed
     */
    protected $dao = null;
    protected $sql = '';
    private $outerSql = '';
    protected $where = '';
    protected $rawData = array();
    protected $rawDataString = '';
    protected $rawDataMultiple = array();

    protected function initManager()
    {
        $this->api = $this->config->getString('database_api');
        $this->driver = $this->config->getString('db_driver');
        $this->host = $this->config->getString('db_host');
        $this->username = $this->config->getString('db_username');
        $this->password = $this->config->getString('db_password');
        $this->databaseName();
        $this->setTuple();
    }

    private function databaseName()
    {
        /*
         * Get entity name from the name of the entity manager.
         * 1) get class name from full class path.
         * 2) get the position of the second uppercase letter.
         * 3) get the entity name
         */
        $className = substr(get_class($this), strrpos(get_class($this), '\\')+1);
        preg_match('/[A-Z]/', substr($className, 1), $matches, PREG_OFFSET_CAPTURE);
        $positionEndEntityName = $matches[0][1];
        $entityName = substr($className, 0, $positionEndEntityName+1);
        if (($this->databaseName = $this->config->getString('db_'.strtolower($entityName))) == null) {
            $this->databaseName = $this->config->getString('db_default');
        }
    }

    public function dao()
    {
        if ($this->dao == null) {
            $this->initDao();
        }
        try {
            $this->checkDao();
        } catch (Exception $exception) {
            $this->logger->logException($exception);
            die('Could not connect to the database.');
        }
        return $this->dao;
    }

    private function initDao()
    {
        $factory = '\\Library\\'.$this->api.'Factory';
        $this->dao = $factory::getConnection(
            $this->driver,
            $this->host,
            $this->databaseName,
            $this->username,
            $this->password
        );
    }

    /**
     * Throws Exception if DAO is not set.
     * @throws Exception
     **/
    private function checkDao()
    {
        if ($this->dao == null) {
            $logMessage = "No database access object (DAO) found: DRIVER:{$this->driver}".
                ":::HOST:{$this->host}:::DATABASE:{$this->databaseName}:::USERNAME:{$this->username}";
            throw new Exception($logMessage);
        }
    }

    private function closeConnection()
    {
        $this->statement = null;
        $this->dao = null;
    }

    abstract protected function setTuple();

    protected function tupleName()
    {
        return $this->tupleName;
    }

    protected function tupleAlias()
    {
        return $this->tupleAlias;
    }

    protected function setTupleName($tupleName)
    {
        $this->tupleName = (string)$tupleName;
    }

    protected function setTupleAlias($tupleAlias)
    {
        $this->tupleAlias = (string)$tupleAlias;
    }

    private function getPdoType($value)
    {
        return $this->mapTypesToPdo(gettype($value));
    }

    private function mapTypesToPdo($typeName)
    {
        $pdoTypeId = PDO::PARAM_STR;
        switch($typeName)
        {
            case "integer":
                $pdoTypeId = PDO::PARAM_INT;
                break;
            case "boolean":
                $pdoTypeId = PDO::PARAM_BOOL;
                break;
            default:
                $pdoTypeId = PDO::PARAM_STR;
                break;
        }
        return $pdoTypeId;
    }

    protected function bindValue($placeHolder, $value)
    {
        $this->valuesToBind[] = array(":$placeHolder", $value, $this->getPdoType($value));
    }

    private function rawDataErrorString()
    {
        $this->rawDataError('string');
    }

    private function rawDataError($type = 'array')
    {
        //fetch() make no difference between failure and empty data!
        if ($this->rawData === false) {
            switch($type)
            {
                case 'string':
                    $this->rawData = '';
                    break;
                default:
                    $this->rawData = array();
                    break;
            }
        }
    }

    protected function querySingle()
    {
        $this->query();
        if (!$this->errorEncountered()) {
            $this->statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->rawData = $this->statement->fetch(); //return false on failure AND on empty data!!
            $this->rawDataError();
        }
        $this->closeConnection();
    }

    protected function querySingleClass($className)
    {
        $this->query();
        if (!$this->errorEncountered() && $this->querySetFetchClass($className)) {
            $this->rawData = $this->statement->fetch();
            $this->rawDataError();
        }
        $this->closeConnection();
    }

    private function querySetFetchClass($className)
    {
        if (!($errorEncountered = !class_exists($className))) {
            $this->logger->logError(__METHOD__.':'.__LINE__.": $className does not exist.");
        } else {
            $this->statement->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $className);
        }
        return $errorEncountered;
    }

    protected function querySingleColumn($columnNumber = 0)
    {
        $this->query();
        if (!$this->errorEncountered()) {
            $this->rawDataString = $this->statement->fetchColumn($columnNumber);
            if ($this->rawDataString === false) {
                $this->rawDataString = '';
            }
            $this->rawDataError();
        }
        $this->closeConnection();
    }

    private function rawDataMultipleError()
    {
        if ($this->rawDataMultiple === false) {
            $this->rawDataMultiple = array();
        }
    }

    protected function queryMultiple()
    {
        $this->query();
        if (!$this->errorEncountered()) {
            $this->statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->rawDataMultiple = $this->statement->fetchAll();
            $this->rawDataMultipleError();
        }
        $this->closeConnection();
    }

    protected function queryMultipleClass($className)
    {
        $this->query();
        if (!$this->errorEncountered() && $this->querySetFetchClass($className)) {
            $this->rawDataMultiple = $this->statement->fetchAll();
            $this->rawDataMultipleError();
        }
        $this->closeConnection();
    }

    protected function queryMultipleColumns($columnNumber = 0)
    {
        $this->query();
        if (!$this->errorEncountered()) {
            $this->statement->setFetchMode(PDO::FETCH_COLUMN, $columnNumber);
            $this->rawDataMultiple = $this->statement->fetchAll();
            $this->rawDataMultipleError();
        }
        $this->closeConnection();
    }

    private function query()
    {
        $this->from();
        $this->joins();
        $this->where();
        $this->groupBy();
        $this->having();
        $this->orderBy();
        $this->limit();
        if ($this->outerSql) {
            $this->sql = sprintf($this->outerSql, $this->sql);
        }
        $this->accessDatabase();
    }

    /**
     * Insert/update/delete
     */
    private function execute()
    {
        $this->where();
        $this->accessDatabase();
        $this->closeConnection();
    }

    protected function resetQuery()
    {
        $this->outerSql = '';
        $this->sql = '';
        $this->joins = array();
        $this->where = '';
        $this->anonymousValuesStrings = array();
        $this->valuesToBind = array();
        $this->valuesToBindCount = 1;
        $this->groupByAttributes = array();
        $this->havingCount = 0;
        $this->orderBy = array();
        $this->limitOffset = 0;
        $this->limitCount = 0;
    }

    private function accessDatabase()
    {
        $this->valuesToBind_DEBUG = '';
        try {
            $this->sql = trim($this->sql);
            $this->statement = $this->dao()->prepare($this->sql);
            foreach ($this->valuesToBind as $value) {
                $paramType = isset($value[2]) ? $value[2] : PDO::PARAM_STR;
                $this->statement->bindValue($value[0], $value[1], $paramType);
                $this->valuesToBind_DEBUG .= "Value {$value[0]} (type: $paramType)=> {$value[1]} END;";
            }
            $this->logger->logMessage($this->logQuery());
            if ($this->debug) {
                var_dump($this->sql);
                var_dump($this->valuesToBind);
            }
            $this->setErrorEncountered(!$this->statement->execute());
            $this->setLastInsertId();
        } catch (PDOException $exception) {
            $this->setErrorEncountered(true);
            $this->logger->logException($exception);
            $this->logger->logError($this->logQuery());
        }
        $this->resetQuery();
    }

    private function logQuery()
    {
        return __METHOD__.':'.__LINE__.': SQL'.PHP_EOL.$this->sql.PHP_EOL.'END SQL'.
            ($this->valuesToBind_DEBUG ? ' ### BINDED VALUES'.PHP_EOL.
            $this->valuesToBind_DEBUG.PHP_EOL.'END BINDED VALUES' : '');
    }

    private function setValuesToBind()
    {
        foreach ($this->attributeNamesForSaving as $attributeName) {
            $getterMethodName = $attributeName;
            $format = null;
            // Get dates as strings. @use Entity->dateAsString conversion method.
            if (gettype($this->entity()->$getterMethodName()) == 'object') {
                $getterMethodName = 'dateAsString';
                $format = 'YmdHis';
                $value = $this->entity()->$getterMethodName($attributeName, $format);
            } else {
                $value = $this->entity()->$getterMethodName($format);
            }
            $this->valuesToBind[] = array($this->valuesToBindCount, $value, $this->getPdoType($value));
            $this->valuesToBindCount++;
        }
    }

    protected function databaseUpdate(Entity $entity)
    {
        $attributeNames = $entity->attributeNames();
        $this->attributeNamesForSaving = array_diff($attributeNames, array('id', 'createdAt', 'createdBy'));
        $this->sql = '
            UPDATE '.$this->tupleName.' '.$this->tupleAlias.'
            SET
                '.implode(' = ?, ', $this->attributeNamesForSaving).' = ?
            WHERE
                '.$this->tupleAlias.'.'.$this->updateAttribute.' = ?
        ';
        $this->setEntity($entity);
        $this->setValuesToBind();
        $updateAttributeGetter = $this->updateAttribute;
        $this->valuesToBind[] = array($this->valuesToBindCount, $entity->$updateAttributeGetter(), PDO::PARAM_INT);
        //var_dump($this->sql);die;
        //var_dump($this->valuesToBind);die;
        $this->execute();
    }

    protected function setDeleteAttribute($attributeName)
    {
        $this->deleteAttribute = $attributeName;
        return $this;
    }

    protected function databaseDeleteMultiple(array $ids)
    {
        if (!empty($ids)) {
            $this->addWherePart($this->deleteAttribute.' IN ('.implode(',', $ids).')');
            $this->databaseDelete();
        }
    }

    protected function databaseDeleteSingle($id)
    {
        $this->addWherePart($this->deleteAttribute.' = :id');
        $this->valuesToBind[] = array(':id', $id, PDO::PARAM_INT);
        $this->databaseDelete();
    }

    private function databaseDelete()
    {
        $this->sql = 'DELETE FROM '.$this->tupleName();
        $this->execute();
    }

    /**
     * SQL Server: returns an array of tuple attributes.
     * @param string $tupleName
     * @return array
     */
    public function getAttributesOf($tupleName)
    {
        $attributes = array();
        $sql = '
            SELECT
                column_name AS attributeName,
                data_type AS dataType,
                character_maximum_length AS maxLength
            FROM information_schema.columns
            WHERE
                table_name = :tupleName
        ';
        $fetchOptions = array(&$attributes);
        $valuesToBind = array(
            array(':tupleName', $tupleName),
        );
        $this->executeQuery($sql, $fetchOptions, $valuesToBind);
        return $attributes;
    }

    protected function generateSelectString(array $attributeNames)
    {
        $selectParts = array();
        foreach ($attributeNames as $tupleAlias => $array) {
            if (!$tupleAlias) {
                $tupleAlias = $this->tupleAlias();
            }
            $selectParts[] = $this->generateSelectStringByAlias($array, $tupleAlias);
        }
        return implode(', ', $selectParts);
    }

    private function generateSelectStringByAlias(array $attributeNames, $tupleAlias = '')
    {
        if ($tupleAlias == '') {
            $tupleAlias = $this->tupleAlias();
        }
        return implode(', ', $this->generateSelect($attributeNames, $tupleAlias));
    }

    private function generateSelect(array $attributeNames, $tupleAlias)
    {
        $select = array();
        foreach ($attributeNames as $attributeName) {
            $select[] = (array_key_exists($attributeName, $this->selectConditions) ?
                $this->selectConditions[$attributeName] :
                "$tupleAlias.$attributeName")." AS '$tupleAlias.$attributeName'";
        }
        return $select;
    }

    protected function addSelectCondition($attributeName, $condition, $action1, $action2 = '')
    {
        $this->selectConditions[$attributeName] = "CASE WHEN $condition THEN $action1".
            ($action2 ? " ELSE $action2" : "")." END";
    }

    protected function lastInsertId()
    {
        return $this->lastInsertId;
    }

    /**
     * Store the last inserted id.
     * Must be called before destroying the database access object (DAO).
     */
    private function setLastInsertId()
    {
        $this->lastInsertId = (int)$this->dao()->lastInsertId();
    }

    /**
     * SQL Server: returns attributes names from database.
     *
     * @param array $exclude (optional) Names of the attributes to exclude.
     * @return array attributes names
     */
    protected function attributes(array $exclude = array())
    {
        $attributes = array();
        $sql = '
            SELECT
                column_name
            FROM information_schema.columns
            WHERE
                table_name = :tupleName
        ';
        $fetchOptions = array(&$attributes, 'all', 'column');
        $valuesToBind = array(
            array(':tupleName', $this->tupleName),
        );
        $this->executeQuery($sql, $fetchOptions, $valuesToBind);
        $attributes = array_diff($attributes, $exclude);
        return $attributes;
    }

    /**
     * Saves $entity in database.
     * If it is a new entity, call self::add, else call self::update.
     * @param Entity $entity
     * @see self::add()
     * @see self::update()
     * @return void
     * @throws RuntimeException if $entity is not valid.
     */
    public function save(Entity $entity)
    {
        if ($entity->isValid()) {
            $entity->isNew() ? $this->add($entity) : $this->update($entity);
        } else {
            throw new RuntimeException('The entity must be valid to be saved in database.');
        }
    }

    private function initInsert()
    {
        $this->entity()->addUnsetAttribute('id');
        $this->attributeNamesForSaving = $this->entity()->attributeNames();
        $this->attributesString = implode(', ', $this->attributeNamesForSaving);
        $this->sql = 'INSERT INTO '.$this->tupleName().' ('.$this->attributesString.') VALUES ';
    }

    private function finalizeInsert()
    {
        $this->sql .= implode(', ', $this->anonymousValuesStrings);
        $this->execute();
    }

    private function setAnonymousValues()
    {
        $this->anonymousValuesStrings[] = '('.preg_replace('/\w+/i', '?', $this->attributesString).')';
        $this->setValuesToBind();
    }

    protected function add(Entity $entity)
    {
        $this->addSingle($entity);
    }

    protected function addSingle(Entity $entity)
    {
        $this->setEntity($entity);
        $this->initInsert();
        $this->setAnonymousValues();
        $this->finalizeInsert();
    }

    protected function addMultiple(array $entities)
    {
        if ($entities) {
            $this->setEntity(reset($entities)); // Returns the first element.
            $this->initInsert();
            foreach ($entities as $entity) {
                $this->setEntity($entity);
                $this->setAnonymousValues();
            }
            $this->finalizeInsert();
        } else {
            $this->logger->logMessage(sprintf("%s:%d: there is no entity to insert.", __METHOD__, __LINE__));
        }
    }

    /**
     * Do not forget to set the tuple name of the relationship, e.g. articlestags.
     *
     * This currently works with array of IDs but it will probably be updated to
     * use array of Entity.
     */
    protected function addRelations()
    {
        $getterMethodName = $this->relationAttributeName;
        if (method_exists($this->entity(), $getterMethodName)) {
            if (($ids = $this->entity()->$getterMethodName())) {
                $this->sql = "INSERT INTO {$this->tupleName()} VALUES ";
                $values = array();
                foreach ($ids as $id) {
                    $values[] = "({$this->entity()->id()}, $id)";
                }
                $this->sql .= implode(', ', $values);
                $this->execute();
            } else {
                $this->logger->logMessage(__METHOD__.':'.__LINE__.": no $getterMethodName to insert.");
            }
        } else {
            $this->logger->logError(
                __METHOD__.':'.__LINE__.': '.get_class($this->entity()).
                "::$getterMethodName does not exist."
            );
        }
    }

    protected function update(Entity $entity)
    {
        $this->databaseUpdate($entity);
    }

    public function delete($entityId)
    {
        $this->databaseDeleteSingle($entityId);
    }

    public function deleteSingle($entityId)
    {
        $this->databaseDeleteSingle($entityId);
    }

    public function deleteMultiple(array $entitiesId)
    {
        $this->databaseDeleteMultiple($entitiesId);
    }

    protected function from()
    {
        $this->sql .= " FROM $this->tupleName $this->tupleAlias";
    }

    private function incrementJoinId()
    {
        $this->joinId++;
    }

    protected function addJoinCondition($joinCondition)
    {
        $this->joinConditions[$this->joinId] = $joinCondition;
    }

    protected function addLeftJoin(
        $otherTupleName,
        $otherTupleAlias,
        $otherTupleAttributeName,
        $attributeName,
        $tupleAlias = ''
    ) {
        $this->joinType = 'LEFT';
        $this->addJoin(
            $otherTupleName,
            $otherTupleAlias,
            $otherTupleAttributeName,
            $attributeName,
            $tupleAlias
        );
    }

    //@todo handle condition on join
    protected function addInnerJoin(
        $otherTupleName,
        $otherTupleAlias,
        $otherTupleAttributeName,
        $attributeName,
        $tupleAlias = ''
    ) {
        $this->joinType = 'INNER';
        $this->addJoin(
            $otherTupleName,
            $otherTupleAlias,
            $otherTupleAttributeName,
            $attributeName,
            $tupleAlias
        );
    }

    /**
     * If $tupleAlias is empty, uses the current manager one.
     */
    private function addJoin(
        $otherTupleName,
        $otherTupleAlias,
        $otherTupleAttributeName,
        $attributeName,
        $tupleAlias = ''
    ) {
        if ($tupleAlias == '') {
            $tupleAlias = $this->tupleAlias;
        }
        $this->incrementJoinId();
        $this->joins[$this->joinId] = " $this->joinType JOIN $otherTupleName ".
            "$otherTupleAlias ON $otherTupleAlias.$otherTupleAttributeName = ".
            "$tupleAlias.$attributeName";
    }

    protected function addJoinRowCount()
    {
        $this->joins[] = 'JOIN (SELECT @currentRow := 0) rows';
    }

    private function joinAttachConditions()
    {
        foreach ($this->joinConditions as $joinId => $joinCondition) {
            if (array_key_exists($joinId, $this->joins)) {
                $this->joins[$joinId] .= " AND $joinCondition";
            }
        }
    }

    private function joins()
    {
        $this->joinAttachConditions();
        if (!empty($this->joins)) {
            $this->sql .= ' '.implode(PHP_EOL, $this->joins);
        }
    }

    /**
     * Generate an LIMIT clause, starting at $offset and selecting $count results.
     *
     * @param int $offset The offset of the first row is 0.
     * @param int $count
     * @access private
     * @return void
     */
    protected function limit()
    {
        if ($this->limitOffset >= 0 && $this->limitCount > 0) {
            $this->sql .= ' LIMIT :limitOffset, :limitCount';
            $this->valuesToBind[] = array(':limitOffset', $this->limitOffset, PDO::PARAM_INT);
            $this->valuesToBind[] = array(':limitCount', $this->limitCount, PDO::PARAM_INT);
        }
    }

    /**
     * Return the total number of items from $this->tupleName.
     * Restore the conditions for the next query.
     */
    public function count()
    {
        $sql = $this->sql;
        $joins = $this->joins;
        $where = $this->where;
        $valuesToBind = $this->valuesToBind;
        $groupByAttributes = $this->groupByAttributes;
        $havingCount = $this->havingCount;
        $orderBy = $this->orderBy;
        $limitOffset = $this->limitOffset;
        $limitCount = $this->limitCount;
        $tupleAlias = $this->tupleAlias();
        if ($this->havingCount) {
            $this->outerSql = 'SELECT COUNT(*) FROM (%s) AS count';
            $this->sql = "SELECT $tupleAlias.id";
            $this->setGroupByAttributes(array('id'));
        } else {
            $this->sql = "SELECT COUNT(DISTINCT $tupleAlias.id)";
            $this->groupByAttributes = array();
        }
        $this->orderBy = array();
        $this->limitOffset = 0;
        $this->limitCount = 0;
        $this->querySingleColumn();
        $this->sql = $sql;
        $this->joins = $joins;
        $this->where = $where;
        $this->valuesToBind = $valuesToBind;
        $this->groupByAttributes = $groupByAttributes;
        $this->havingCount = $havingCount;
        $this->orderBy = $orderBy;
        $this->limitOffset = $limitOffset;
        $this->limitCount = $limitCount;
        return $this->rawDataInt();
    }

    protected function addWherePart($wherePart)
    {
        if ($wherePart != '') {
            if ($this->where != '') {
                $this->where .= ' AND ('.$wherePart.')';
            } else {
                $this->where .= $wherePart;
            }
        }
    }

    protected function where()
    {
        if ($this->where != '') {
            $this->sql .= ' WHERE '.$this->where;
        }
    }

    public function addGroupByAttribute($attributeName, $alias = '')
    {
        if (!$alias) {
            $alias = $this->tupleAlias();
        }
        $this->groupByAttributes[] = "$alias.$attributeName";
        return $this;
    }

    /**
     * If no alias set, use self::tupleAlias.
     * @param array $groupByAttributes
     * @param string $alias = ''
     * @return self
     */
    public function setGroupByAttributes(array $groupByAttributes, $alias = '')
    {
        foreach ($groupByAttributes as $attributeName) {
            $this->addGroupByAttribute($attributeName, $alias);
        }
        return $this;
    }

    protected function groupBy()
    {
        if ($this->groupByAttributes) {
            $this->sql .= ' GROUP BY '.implode(', ', $this->groupByAttributes);
        }
    }

    protected function having()
    {
        if ($this->havingCount) {
            $this->sql .= ' HAVING count('.$this->tupleAlias().'.id) = '.$this->havingCount;
        }
    }

    protected function orderBy()
    {
        if (!empty($this->orderBy)) {
            $this->sql .= ' ORDER BY '.implode(',', $this->orderBy);
        }
    }

    protected function addOrderBy($attributeName, $isAscending = true)
    {
        $direction = $isAscending ? 'ASC' : 'DESC';
        $this->orderBy[] = "$attributeName $direction";
    }

    protected function getSingleAttributeById($attributeName, $id, $idAttributeName = 'id')
    {
        $this->sql = "SELECT $attributeName";
        $this->addWherePart("$idAttributeName = :id");
        $this->bindValue('id', $id);
        $this->querySingleColumn();
        return $this->rawDataString();
    }

    protected function getUnique($entityId)
    {
        $this->sql = 'SELECT * FROM '.$this->tupleName().' WHERE id = :id';
        $this->bindValue('id', $entityId);
        $this->querySingle();
    }

    /**
     * @param $limitOffset
     * @return self
     */
    public function setLimitOffset($limitOffset)
    {
        $this->limitOffset = $limitOffset;
        return $this;
    }

    /**
     * @param $limitCount
     * @return self
     */
    public function setLimitCount($limitCount)
    {
        $this->limitCount = $limitCount;
        return $this;
    }

    protected function havingCount()
    {
        return $this->havingCount;
    }

    /**
     * @param $havingCount
     * @return self
     */
    protected function setHavingCount($havingCount)
    {
        $this->havingCount = $havingCount;
        return $this;
    }

    /**
     * @return string
     */
    protected function relationAttributeName()
    {
        return $this->relationAttributeName;
    }

    /**
     * Set the name of the attribute, in the Entity currently set, which contains
     * the IDs referring to the related tuple.
     * @param string $relationAttributeName
     * @return self
     */
    public function setRelationAttributeName($relationAttributeName)
    {
        $this->relationAttributeName = $relationAttributeName;
        return $this;
    }

    /**
     * @return self
     */
    public function setDebugOn()
    {
        $this->debug = true;
        return $this;
    }

    /**
     * @param $updateAttribute
     * @return self
     */
    public function setUpdateAttribute($updateAttribute)
    {
        $this->updateAttribute = $updateAttribute;
        return $this;
    }
}
