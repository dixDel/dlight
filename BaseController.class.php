<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \InvalidArgumentException;
use \Library\Interfaces\IApplication;
use \Library\Manager;
use \Library\Page;
use \RuntimeException;

/**
 * Description of BackController
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class BaseController extends ApplicationComponent
{
    protected $action = '';
    protected $bundle = '';
    private $applicationCode = 1;
    private $messages = array();
    private $view = '';

    public function __construct(IApplication $app, $controllerAction, $controllerBundle)
    {
        parent::__construct($app);
        $this->setAction($controllerAction);
        $this->setBundle($controllerBundle);
        $this->setView($this->action);
        $this->app->page()->addVar('user', $this->app->user());
        $this->init();
    }

    protected function init()
    {
    }

    public function execute()
    {
        $method = 'execute'.ucfirst($this->action);
        $this->app->errorManager()
            ->logMessage(__METHOD__.':'.__LINE__.": Executing {$this->bundle}::{$this->action}...");
        if (!is_callable(array($this, $method))) {
            throw new RuntimeException('The action "'.$this->action.'" is not defined in this bundle.');
        }
        if ($this->isAuthorisationGranted()) {
            // $this->action can be changed by $this->isAuthorisationGranted
            $method = 'execute'.ucfirst($this->action);
            $this->$method();
        } else {
            $this->httpResponse()->redirect403($this->app->page());
        }
    }

    abstract protected function isAuthorisationGranted();

    public function setBundle($bundle)
    {
        if (!is_string($bundle) || empty($bundle)) {
            throw new InvalidArgumentException('The bundle must be a valid string.');
        }
        $this->bundle = $bundle;
    }

    public function setApplicationCode($code)
    {
        $this->applicationCode = $code;
    }

    public function applicationCode()
    {
        return $this->applicationCode;
    }

    public function setAction($action)
    {
        if (!is_string($action)||empty($action)) {
            throw new InvalidArgumentException('The action must be a valid string.');
        }
        $this->action = $action;
    }

    public function setView($view)
    {
        if (!is_string($view) || empty($view)) {
            throw new InvalidArgumentException('The view must be a valid string.');
        }
        $this->view = $view;
        $this->app->page()->setContentFile(
            $this->app->config()->getString('bundlesPath').
            $this->bundle.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.$this->view.'.php'
        );
    }
}
