<?php
namespace Library\Tests;

use \Library\Tests\PHPUnit_Extensions_Database_Operation_MySQL55Truncate;
use \PDO;

/**
 *
 **/
abstract class DbUnitTestCase extends \PHPUnit_Extensions_Database_TestCase
{
    private $db = null;
    private $dataSetXmlFileName = '';

    abstract protected function init();

    //public function setUp()
    //{
        // Solve data set always empty:
        // @source https://stackoverflow.com/questions/17251164/phpunit-with-dbunit-initial-fixture-fails-to-set-up
        //parent::setUp();
    //}

    /**
     * Override
     */
    public function getConnection()
    {
        return $this->createDefaultDBConnection($this->db, $this->config->getString('db_default'));
    }

    /**
     * Override
     */
    public function getSetUpOperation()
    {
        $cascadeTruncates = false;
        return new \PHPUnit_Extensions_Database_Operation_Composite(
            array(
                new PHPUnit_Extensions_Database_Operation_MySQL55Truncate($cascadeTruncates),
                \PHPUnit_Extensions_Database_Operation_Factory::INSERT()
            )
        );
    }

    /**
     * Override
     */
    public function getDataSet()
    {
        $appRoot = dirname(dirname(__DIR__));
        $testsDir = $appRoot.DIRECTORY_SEPARATOR.'Tests'.DIRECTORY_SEPARATOR.'DataSets'.DIRECTORY_SEPARATOR;
        return $this->createXMLDataSet($testsDir.$this->dataSetXmlFileName.'.xml');
    }

    /**
     * @param $db
     * @return self
     */
    public function setDb(PDO $db)
    {
        $this->db = $db;
        return $this;
    }

    /**
     * @param $dataSetXmlFileName
     * @return self
     */
    public function setDataSetXmlFileName($dataSetXmlFileName)
    {
        $this->dataSetXmlFileName = $dataSetXmlFileName;
        return $this;
    }
}
