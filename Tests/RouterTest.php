<?php
namespace Tests;

use \Library\Factory;

/**
 * Test Router.
 **/
class RouterTest extends \PHPUnit_Framework_TestCase
{
    private $mappingName = 'IRouter';
    private $routerFullyQualifiedClassName = '\\Library\\Router';
    private $configVarName = 'routesFilePath';
    private $someRouteXmlFile = 'Tests/Config/fakeRoutes.xml';
    private $someRouteUrl = 'bidon';
    private $routeFullyQualifiedClassName = '\\Library\\Route';
    private $routeNotSet = 'notset';

    public function setUp()
    {
        $this->factory = new Factory;
        $this->factory->addMapping('\\Library\\Interfaces\\ICache', '\\Library\\Fakes\\FakeCache');
        $this->factory->addMapping($this->mappingName, $this->routerFullyQualifiedClassName);
        $this->factory->config()->addVar($this->configVarName, $this->someRouteXmlFile);
        $this->router = $this->factory->get($this->mappingName);
    }

    /**
     * @test
     */
    public function getRoute_routesInitializedFromConfigFile_returnRequestedRoute()
    {
        $result = $this->router->getRoute($this->someRouteUrl);
        $this->assertInstanceOf($this->routeFullyQualifiedClassName, $result);
    }

    /**
     * @test
     * @expectedException RuntimeException
     */
    public function getRoute_routeNotSet_throwRuntimeException()
    {
        $this->router->getRoute($this->routeNotSet);
    }
}
