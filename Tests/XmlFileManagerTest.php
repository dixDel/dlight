<?php
namespace Tests;

use \Library\Factory;
use \Library\FileManagers\XmlFileManager;

class XmlFileManagerTest extends \PHPUnit_Framework_TestCase
{
    private $xmlFileManager = null;
    private $xmlFileWithOneTag = 'Tests/parseXmlToKeyValueArrayTest.xml';
    private $someExistingAttributeAsKey = 'key';
    private $someExistingAttributeAsValue = 'class';
    private $xmlFileWithThreeTags = 'Tests/parseXmlToKeyValueArrayTest2.xml';
    private $expectedAttribute = 'StringUtils';
    private $expectedValue = 'Library/Utils/StringUtils';

    public function setUp()
    {
        $factory = new Factory;
        $config = new \Library\Config('dlightTestConfigPath');
        $logger = new \Library\Fakes\FakeErrorManager('');
        $cache = $factory->getFake('Cache');
        $stringUtils = new \Library\Fakes\FakeStringUtils();
        $this->xmlFileManager = new XmlFileManager($config, $logger, $cache, $stringUtils);
        $this->somePath = $config->getString('testFilesPath');
        $this->someRootTagName = 'rootTag';
        $this->someFilename = 'saved.xml';
    }

    /**
     * @test
     */
    public function parseXmlToKeyValueArray_nonExistentFile_returnArray()
    {
        $result = $this->xmlFileManager->parseXmlToKeyValueArray('');
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function parseXmlToKeyValueArray_nonExistentFile_returnEmptyArray()
    {
        $result = $this->xmlFileManager->parseXmlToKeyValueArray('');
        $this->assertEmpty($result);
    }

    /**
     * @test
     */
    public function parseXmlToKeyValueArray_xmlFileWithOneTagTwoAttributes_returnArrayOfLengthOne()
    {
        $result = $this->xmlFileManager->parseXmlToKeyValueArray($this->xmlFileWithOneTag, $this->someExistingAttributeAsKey, $this->someExistingAttributeAsValue);
        $this->assertCount(1, $result);
    }

    /**
     * @test
     */
    public function parseXmlToKeyValueArray_xmlFileWithThreeTagsTwoAttributes_returnArrayOfLengthThree()
    {
        $result = $this->xmlFileManager->parseXmlToKeyValueArray($this->xmlFileWithThreeTags, $this->someExistingAttributeAsKey, $this->someExistingAttributeAsValue);
        $this->assertCount(3, $result);
    }

    /**
     * @test
     */
    public function parseXmlToKeyValueArray_xmlFileWithOneTagTwoAttributes_returnArrayWithExpectedAttribute()
    {
        $result = $this->xmlFileManager->parseXmlToKeyValueArray($this->xmlFileWithOneTag, $this->someExistingAttributeAsKey, $this->someExistingAttributeAsValue);
        $this->assertArrayHasKey($this->expectedAttribute, $result);
    }

    /**
     * @test
     */
    public function parseXmlToKeyValueArray_xmlFileWithOneTagTwoAttributes_returnArrayWithExpectedKeyValue()
    {
        $result = $this->xmlFileManager->parseXmlToKeyValueArray($this->xmlFileWithOneTag, $this->someExistingAttributeAsKey, $this->someExistingAttributeAsValue);
        $this->assertEquals($this->expectedValue, $result[$this->expectedAttribute]);
    }

    /**
     * @test
     */
    public function saveXmlFile_emptyRootElement_doNotSaveFile()
    {
        $this->xmlFileManager->initRoot($this->someRootTagName);
        $this->xmlFileManager->saveXmlFile($this->somePath, $this->someFilename);
        $this->assertFileNotExists($this->somePath.'/'.$this->someFilename);
    }

    /**
     * @test
     */
    public function saveXmlFile_emptyRootElementWithFlagSaveEmptyRoot_saveFile()
    {
        $this->xmlFileManager->initRoot($this->someRootTagName);
        $this->xmlFileManager->saveXmlFile($this->somePath, $this->someFilename, true);
        $this->assertFileExists($this->somePath.'/'.$this->someFilename);
    }

    public function tearDown()
    {
        $fileToDelete = $this->somePath.'/'.$this->someFilename;
        if (file_exists($fileToDelete)) {
            unlink($fileToDelete);
        }
    }
}
