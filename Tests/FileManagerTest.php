<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Tests;

use \Library\Config;
use \Library\Fakes\FakeCache;
use \Library\Fakes\FakeErrorManager;
use \Library\Fakes\FakeStringUtils;
use \Library\FileManager;

/**
 * Unit test for FileManager
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class FileManagerTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $config = new Config('dlightTestConfigPath');
        $logger = new FakeErrorManager('inexistantLog');
        $cache = new FakeCache($logger);
        $stringUtils = new FakeStringUtils;
        $this->fileManager = new FileManager($config, $logger, $cache, $stringUtils);
        $this->testFilesPath = $config->getString('testFilesPath');
        $this->someFileName = 'deleteMe';
        $this->someFileToDelete = "{$this->testFilesPath}/{$this->someFileName}";
        $this->someFileToDelete2 = "{$this->testFilesPath}/deleteMeToo";
        $this->someFileSuffix = 'xml';
        $this->arrayFileFullPathsToDelete = array();
    }

    /**
     * @test
     */
    public function deleteFiles_arrayOneFileName_deleteThatFile()
    {
        $this->initFileToDelete($this->someFileToDelete);
        $this->fileManager->deleteFiles($this->arrayFileFullPathsToDelete);
        $this->assertFileNotExists($this->someFileToDelete);
    }

    /**
     * @test
     */
    public function deleteFiles_arrayTwoFileNames_deleteThoseFiles()
    {
        $this->initFileToDelete($this->someFileToDelete);
        $this->initFileToDelete($this->someFileToDelete2);
        $this->fileManager->deleteFiles($this->arrayFileFullPathsToDelete);
        $this->assertFileNotExists($this->someFileToDelete);
        $this->assertFileNotExists($this->someFileToDelete2);
    }

    /**
     * @test
     */
    public function deleteFiles_arrayOneFileNameSuffixSet_deleteThatFile()
    {
        $fileFullPath = $this->someFileToDelete.'.'.$this->someFileSuffix;
        touch($fileFullPath);
        $this->assertFileExists($fileFullPath);
        $this->arrayFileFullPathsToDelete[] = $this->someFileToDelete;
        $this->fileManager->setFileSuffix($this->someFileSuffix);
        $this->fileManager->deleteFiles($this->arrayFileFullPathsToDelete);
        $this->assertFileNotExists($fileFullPath);
    }

    /**
     * @test
     */
    public function deleteFiles_arrayOneFileNamePathSet_deleteThatFile()
    {
        touch($this->someFileToDelete);
        $this->assertFileExists($this->someFileToDelete);
        $this->arrayFileFullPathsToDelete[] = $this->someFileName;
        $this->fileManager->setFilePath($this->testFilesPath);
        $this->fileManager->deleteFiles($this->arrayFileFullPathsToDelete);
        $this->assertFileNotExists($this->someFileToDelete);
    }

    private function initFileToDelete($fileFullPath)
    {
        touch($fileFullPath);
        $this->assertFileExists($fileFullPath);
        $this->arrayFileFullPathsToDelete[] = $fileFullPath;
    }

    public function tearDown()
    {
        foreach ($this->arrayFileFullPathsToDelete as $fileFullPath) {
            if (file_exists($fileFullPath)) {
                unlink($fileFullPath);
            }
        }
    }
}
