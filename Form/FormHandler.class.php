<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form;

use \Library\ApplicationComponent;
use \Library\Entity;
use \Library\Form\Form;
use \Library\Interfaces\IApplication;
use \Library\Interfaces\IFormHandler;
use \Library\Utils\StringUtils;

/**
 * Description of FormHandler
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class FormHandler extends ApplicationComponent implements IFormHandler
{
    protected $entity = null;
    protected $stringUtils = null;
    private $formName = '';
    private $debug = false;
    protected $manager = null;
    private $rawData = array();
    private $embeddedFormName = '';
    private $embeddedFormCount = 0;
    private $entities = array();

    public function __construct(IApplication $app, Entity $entity, StringUtils $stringUtils, $formName)
    {
        parent::__construct($app);
        $this->setEntity($entity);
        $this->stringUtils = $stringUtils;
        $this->setFormName('token-'.$formName);
        $this->setManager();
    }

    public function process()
    {
        $isSuccess = false;
        // Check if there is POST data and the submitted form is the one called formName.
        // This avoid displaying the error message when several forms are on the same page.
        if ($this->app->httpRequest()->method() == 'POST' && $this->app->httpRequest()->postExists($this->formName)) {
            // If embeddedFormName is set, this is a subform so we don't check the csrf.
            if (
                $this->debug() ||
                $this->embeddedFormName ||
                ($this->app->user()->isCsrfValid($this->app->httpRequest()->postDataString($this->formName)))
            ) {
                $this->processRawData();
                if (!($isSuccess = $this->entity->isValid())) {
                    $this->app->user()->addFlash('Please fix the errors and try again.');
                }
            } else {
                $this->app->user()->addFlash('A security check has failed. Please retry to submit your data.');
            }
        }
        return $isSuccess;
    }

    protected function manager()
    {
        return $this->manager;
    }

    protected function setManager()
    {
        $entityClassName = $this->stringUtils->className($this->entity);
        $this->manager = $this->app->factory()->get('Application/Models/'.$entityClassName.'Manager');
    }

    /**
     * Override this to run additional actions on the entity.
     */
    protected function processCustom()
    {
    }

    private function processRawDataMultiple()
    {
        $rawPostData = $this->app->httpRequest()->postDataArray($this->embeddedFormName);
        foreach ($rawPostData as $embeddedFormCount => $embeddedFormData) {
            $this->embeddedFormCount = $embeddedFormCount;
            $this->processRawDataSingle();
            // Objects are passed by reference, so clone it to avoid its data being overwritten
            $this->entities[$this->embeddedFormCount] = clone $this->entity;
            $this->processCustom();
        }
    }

    private function processRawDataSingle()
    {
        // @todo unify with Manager::castEntity
        foreach ($this->entity->attributeNames() as $attributeName) {
            // @todo postExists
            $this->rawData[$attributeName] = $this->postData($attributeName);
            if (
                gettype($this->entity->$attributeName()) === 'object' &&
                $this->stringUtils->className($this->entity->$attributeName()) === 'DateTime'
            ) {
                $this->createDate($attributeName);
            }
        }
        $this->manager()->setEntity($this->entity);
        $this->manager()->setRawData($this->rawData);
        $this->manager()->castEntity();
        $this->setEntity($this->manager()->entity());
    }

    protected function processRawData()
    {
        $this->rawDataReset();
        if ($this->embeddedFormName) {
            $this->processRawDataMultiple();
        } else {
            $this->processRawDataSingle();
        }
    }

    private function createDate($attributeName)
    {
        $dateArray = $this->rawData[$attributeName];
        if (!$dateArray) {
            $dateArray = array();
        }
        extract($dateArray);
        if (isset($year, $month, $day)) {
            $this->rawData[$attributeName] = "$year-$month-$day 00:00:00";
        } else {
            unset($this->rawData[$attributeName]);
        }
    }

    private function postData($attributeName)
    {
        $postData = '';
        if ($this->embeddedFormName) {
            $postData = $this->app->httpRequest()->postData(
                $this->embeddedFormName,
                $this->embeddedFormCount,
                $attributeName
            );
        } else {
            $postData = $this->app->httpRequest()->postData($attributeName);
        }
        return $postData;
    }

    public function entity()
    {
        return $this->entity;
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * @param $formName
     * @return self
     */
    public function setFormName($formName)
    {
        $this->formName = $formName;
        return $this;
    }

    /**
     * @return debug
     */
    private function debug()
    {
        return $this->debug;
    }

    /**
     * @return self
     */
    public function setDebugOn()
    {
        $this->debug = true;
        return $this;
    }

    /**
     * @return self
     */
    public function setDebugOff()
    {
        $this->debug = false;
        return $this;
    }

    protected function rawData()
    {
        return $this->rawData;
    }

    private function rawDataReset()
    {
        $this->rawData = array();
    }

    /**
     * @param $embeddedFormName
     * @return self
     */
    public function setEmbeddedFormName($embeddedFormName)
    {
        $this->embeddedFormName = $embeddedFormName;
        return $this;
    }

    /**
     * @param $embeddedFormCount
     * @return self
     */
    public function setEmbeddedFormCount($embeddedFormCount)
    {
        $this->embeddedFormCount = $embeddedFormCount;
        return $this;
    }

    protected function currentEntity()
    {
        return $this->entities[$this->embeddedFormCount];
    }

    /**
     * @return entities
     */
    public function entities()
    {
        return $this->entities;
    }

    /**
     * @return embeddedFormCount
     */
    public function embeddedFormCount()
    {
        return $this->embeddedFormCount;
    }
}
