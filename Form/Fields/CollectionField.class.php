<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form\Fields;

use \Library\Form\Field;
use \Library\Form\FormBuilder;

/**
 * Field to contain multiple forms.
 * Used to embed forms into other forms.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class CollectionField extends Field
{
    private $formBuilders = array();
    private $prototype = '';
    private $legend = '';
    private $legendClass = '';
    private $legends = array();
    private $fieldsetClass = '';
    private $hasDeleteButton = false;

    public function setLegendClass($legendClass)
    {
        $this->legendClass = $legendClass;
    }

    public function legends()
    {
        return $this->legends;
    }

    public function legendByCount($count)
    {
        $legend = '';
        if (array_key_exists($count, $this->legends)) {
            $legend = $this->legends[$count];
        }
        return $legend;
    }

    public function setLegends(array $legends)
    {
        $this->legends = $legends;
    }

    private function fieldsetClass()
    {
        return $this->fieldsetClass;
    }

    public function setFieldsetClass($fieldsetClass)
    {
        $this->fieldsetClass = $fieldsetClass;
    }

    public function widgets()
    {
        $widgets = array();
        $formCount = 0;
        foreach ($this->formBuilders as $formBuilder) {
            $formBuilder->setFormNamePrefix($this->name().'['.$formCount.']');
            $formBuilder->build();
            $formFields = $formBuilder->form()->fields();
            $widgets[$formCount] = $formFields;
            $formCount++;
        }
        return $widgets;
    }

    public function buildWidget()
    {
        $widget = '';
        $cnt = 0;
        foreach ($this->formBuilders as $formBuilder) {
            $widget .= '<fieldset class="integratedForm '.$this->fieldsetClass.'">';
            if ($this->legend != '') {
                $legendTemplate = '<legend class="$$legendClass$$">$$legendText$$</legend>';
                $widget .= str_replace(
                    array(
                        '$$legendClass$$',
                        '$$legendText$$',
                    ),
                    array(
                        $this->legendClass,
                        $this->legend.' '.($cnt+1),
                    ),
                    $legendTemplate
                );
            } elseif (!empty($this->legends)) {
                if (array_key_exists($cnt, $this->legends)) {
                    $widget .= "<legend>{$this->legends[$cnt]}</legend>";
                }
            }
            $formBuilder->setFormNamePrefix($this->name.'['.$cnt.']');
            $formBuilder->build();
            $form = $formBuilder->form()->fields();
            foreach ($form as $field) {
                if (method_exists($field, 'widgets')) {
                    $widget .= '<ul class="skills">';
                    foreach ($field->widgets() as $item) {
                        $item->label()->addClass('skills');
                        $widget .= '<li>'.$item->addClass('hidden')->setLabelAfter().'</li>';
                    }
                    $widget .= '</ul>';
                }
                $widget .= $field->setCreateLabelOff();
            }
            if ($this->hasDeleteButton) {
                $widget .= '<button type="button" class="deleteForm_button">Delete '.
                    strtolower($this->legend).'</button>';
            }
            $widget .= '</fieldset>';
            //if($cnt == 0) {
                //$this->setPrototype($widget);
            //}
            $cnt++;
        }
        $this->finalRender = $widget;
    }

    public function setFormBuilders(array $formBuilders)
    {
        foreach ($formBuilders as $formBuilder) {
            $this->addFormBuilder($formBuilder);
        }
    }

    protected function addFormBuilder(FormBuilder $formBuilder)
    {
        array_push($this->formBuilders, $formBuilder);
    }

    public function prototype()
    {
        return $this->prototype;
    }

    public function setPrototype($prototype)
    {
//        var_dump($prototype);
        $this->prototype = $prototype;
    }

    public function setLegend($legend)
    {
        $this->legend = $legend;
    }

    public function hasDeleteButton()
    {
        return $this->hasDeleteButton;
    }
    public function setHasDeleteButton($hasDeleteButton)
    {
        $this->hasDeleteButton = $hasDeleteButton;
    }
}
