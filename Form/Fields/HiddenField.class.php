<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form\Fields;

use \Library\Form\Fields\InputField;

/**
 * Description of HiddenField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class HiddenField extends InputField
{
    public function __construct(array $options = array())
    {
        $this->setType('hidden');
        parent::__construct($options);
        $this->setCreateLabelOff();
        $this->setNoContainer();
    }
}
