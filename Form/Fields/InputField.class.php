<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form\Fields;

use \Library\Form\Field;

abstract class InputField extends Field
{
    private $template = '<input$$attributes$$/>';

    protected function setType($typeName)
    {
        $this->addAttribute('type', $typeName);
    }

    protected function setPlaceholder($text)
    {
        $this->addAttribute('placeholder', $text);
    }

    protected function setMaxLength($maxLength)
    {
        if ($maxLength > 0) {
            $this->addAttribute('maxLength', (string)$maxLength);
        }
    }

    public function setReadonly()
    {
        $this->addAttribute('readonly', 'readonly');
        parent::setReadonly();
    }

    protected function buildWidget()
    {
        $this->finalRender .= str_replace(
            '$$attributes$$',
            $this->attributesString(),
            $this->template
        );
    }
}
