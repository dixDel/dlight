<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form\Fields;

use \Library\Form\Field;

/**
 * Description of TextField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class TextField extends Field
{
    protected $cols = 70;
    protected $rows = 5;

    public function buildWidget()
    {
        $name = $this->name;
//        if( !empty($this->namePrefix)) {
//            $name = $this->namePrefix.'['.$this->name.']';
//        }
        $widget = '<textarea name="'.$name.'" id="'.$this->id().'"';
        if (!empty($this->cols)) {
            $widget .= ' cols="'.$this->cols.'"';
        }
        if (!empty($this->rows)) {
            $widget .= ' rows="'.$this->rows.'"';
        }
        $widget .= ' class="'.$this->classesString().'"';
        if ($this->readonly) {
            $widget .= ' readonly = "readonly"';
        }
        $widget .= '>';
        $widget .= htmlspecialchars($this->value());
        $this->finalRender = $widget . '</textarea>';
    }

    public function setCols($cols)
    {
        $cols = (int) $cols;
        if ($cols > 0) {
            $this->cols = $cols;
        }
    }

    public function setRows($rows)
    {
        $rows = (int) $rows;
        if ($rows > 0) {
            $this->rows = $rows;
        }
    }

    protected function setStringValue()
    {
        //$this->value = htmlspecialchars(stripslashes($this->value), ENT_NOQUOTES);
    }
}
