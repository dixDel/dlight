<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form\Fields;

use \Library\Form\Field;
use \DateTime;

/**
 * Description of DateField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class DateField extends Field
{
    private $baseId = '';
    protected $months = array(
        '' => 'Month',
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'May',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Aug',
        '09' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
    );

    protected $firstYearFromNow = '+10';
    protected $lastYearFromNow = '-5';
    protected $ascending = true;

    public function __construct(array $options = array())
    {
        if (!isset($options['firstYearFromNow'])) {
            $options['firstYearFromNow'] = $this->firstYearFromNow;
        }
        if (!isset($options['lastYearFromNow'])) {
            $options['lastYearFromNow'] = $this->lastYearFromNow;
        }
        if (!isset($options['ascending'])) {
            $options['ascending'] = $this->ascending;
        }
        parent::__construct($options);
    }

    /**
     * Overrides setValue to accept array or string.
     * An array must contain keys 'month', 'day' and 'year'.
     * @param mixed $value
     */
    public function setValue($value)
    {
        if ($value instanceof DateTime) {
            $dateString = $value->format('Ymd');
            $year = substr($dateString, 0, 4);
            $month = substr($dateString, 4, 2);
            $day = substr($dateString, 6, 2);
            $this->value = $year.$month.$day;
        } elseif (is_array($value)) {
            if (isset($value['month']) && isset($value['day']) && isset($value['year'])) {
                $this->value = $value['year'].$value['month'].$value['day'];
            }
        } else {
            parent::setValue($value);
        }
    }

    private function baseId()
    {
        return $this->baseId;
    }

    /**
     * Override DomElement::setId()
     * @param $id
     * @return self
     */
    public function setId($id)
    {
        $this->baseId = $id;
        return parent::setId($id.'_month');
    }

    public function buildWidget()
    {
        $widget = '';
        $selected_year = 0;
        $selected_month = 0;
        $selected_day = 0;
        if (!empty($this->value) && (int)substr($this->value, 0, 4) > 0) {
            $this->value = htmlspecialchars($this->value);
            $selected_year = substr($this->value, 0, 4);
            $selected_month = substr($this->value, 4, 2);
            $selected_day = substr($this->value, 6, 2);
        }
        $widget .= '<select name="'.$this->name().'[month]" id="'.$this->baseId().'_month" class="select_date">';
        foreach ($this->months as $option_value => $option_text) {
            $widget .= '<option value="'.$option_value.'"';
            if ($option_value == $selected_month) {
                $widget .= ' selected="selected"';
            }
            $widget .= '>'.$option_text.'</option>';
        }
        $widget .= '</select><select name="'.
            $this->name().'[day]" id="'.$this->baseId().'_day" class="select_date"><option value="">Day</option>';
        for ($day = 1; $day <= 31; $day++) {
            $widget .= '<option value="'.($day < 10 ? '0'.$day : $day).'"';
            if ($day == $selected_day) {
                $widget .= ' selected="selected"';
            }
            $widget .= '>'.$day.'</option>';
        }
        $widget .= '</select><select name="'.
            $this->name().'[year]" id="'.$this->baseId().'_year" class="select_date"><option value="">Year</option>';
        $today = new \DateTime('now');
        $thisYear = $today->format('Y');
        $sign = substr($this->firstYearFromNow, 0, 1);
        $nbYears = substr($this->firstYearFromNow, 1); //removes the sign
        if ($sign == '+') {
            $firstYear = $today->add(new \DateInterval('P'.$nbYears.'Y'))->format('Y');
        } else {
            $firstYear = $today->sub(new \DateInterval('P'.$nbYears.'Y'))->format('Y');
        }
        // restore today because sub() modifies the date object it processes.
        $today = new \DateTime('now');
        $sign = substr($this->lastYearFromNow, 0, 1);
        $nbYears = substr($this->lastYearFromNow, 1);
        if ($sign == '+') {
            $lastYear = $today->add(new \DateInterval('P'.$nbYears.'Y'))->format('Y');
        } else {
            $lastYear = $today->sub(new \DateInterval('P'.$nbYears.'Y'))->format('Y');
        }
        if ($this->ascending) {
            for ($year = $lastYear; $year <= $firstYear; $year++) {
                $widget .= '<option value="'.$year.'"';
                if ($year == $selected_year) {
                    $widget .= ' selected="selected"';
                }
                $widget .= '>'.$year.'</option>';
            }
        } else {
            for ($year = $firstYear; $year >= $lastYear; $year--) {
                $widget .= '<option value="'.$year.'"';
                if ($year == $selected_year) {
                    $widget .= ' selected="selected"';
                }
                $widget .= '>'.$year.'</option>';
            }
        }
        $this->finalRender = $widget . '</select>';
    }

    /**
     * @param mixed $firstYearFromNow (+|-)yy (+10, -5, 18 (= +18)...)
     */
    public function setFirstYearFromNow($firstYearFromNow)
    {
        /*
         * If $firstYearOfNow is a string,
         * if matches the pattern (+|-)yy,
         * if there is no sign, adds a +
         */
        if (is_string($firstYearFromNow)) {
            if (preg_match('/^(\+|-)?[0-9]{1,2}$/', $firstYearFromNow)) {
                $this->firstYearFromNow = '';
                $firstChar = substr($firstYearFromNow, 0, 1);
                if ($firstChar != '+' && $firstChar != '-') {
                    $this->firstYearFromNow .= '+';
                }
                $this->firstYearFromNow .= $firstYearFromNow;
            }
        }
    }

    public function setLastYearFromNow($lastYearFromNow)
    {
        if (is_string($lastYearFromNow)) {
            if (preg_match('/^(\+|-)?[0-9]{1,2}$/', $lastYearFromNow)) {
                $this->lastYearFromNow = '';
                $lastChar = substr($lastYearFromNow, 0, 1);
                if ($lastChar != '+' && $lastChar != '-') {
                    $this->lastYearFromNow .= '+';
                }
                $this->lastYearFromNow .= $lastYearFromNow;
            }
        }
    }

    public function setAscending($ascending)
    {
        if (is_bool($ascending)) {
            $this->ascending = $ascending;
        }
    }
}
