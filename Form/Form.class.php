<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form;

use \Library\Entity;
use \Library\Form\Fields\HiddenField;

/**
 * Description of Form
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Form
{
    private $name = '';
    private $namePrefix = '';
    private $csrf = '';
    private $createCsrf = true;
    protected $entity = null;
    protected $entityErrors = array();
    protected $fields = array();

    public function __construct(Entity $entity, $formName)
    {
        $this->setEntity($entity);
        $this->setName('token-'.$formName);
    }

    private function setName($name)
    {
        $this->name = $name;
    }

    public function name()
    {
        return $this->name;
    }

    public function add(Field $field)
    {
        $this->addCsrfField();
        // Get the name of the field.
        $attr = $field->name();
        if (isset($this->entityErrors[$attr])) {
            $field->setErrorMessage($this->entityErrors[$attr]);
        }
        // Assigns the matching value to the field.
        $field->setValue($this->entity->$attr());
        /*
         * Adds name prefix for fields in a collection.
         * Cannot do it in the field name setter,
         * it generates an error because it tries to access the getter of the same name of the entity.
         */
        if ($this->namePrefix) {
            $field->setNameNoPrefix($field->name());
            $fieldName = $this->namePrefix.'['.$field->name().']';
            $field->setName($fieldName);
            $field->setId($fieldName);
            $field->setNamePrefix($fieldName);
        }
        // Adds the field to the list of fields.
        $this->fields[$field->name()] = $field;
        // return the form itself,
        // so I will be able to call this function many times in a row ($form->add($field1)->add($field2)...
        return $this;
    }

    public function isValid()
    {
//        $valid = true;
//        // Checks if all the fields are valid.
//        foreach( $this->fields as $field) {
//            if( !$field->isValid()) {
//                $valid = false;
//            }
//        }
//        return $valid;
//        var_dump($this->entity->isValid());die;
        return $this->entity->isValid();
    }

    public function entity()
    {
        return $this->entity;
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
        $this->setEntityErrors($this->entity->errors());
    }

    public function setEntityErrors($entityErrors)
    {
        $this->entityErrors = $entityErrors;
    }

    public function fields()
    {
        return $this->fields;
    }

    /**
     * Create hidden csrf field if self::createCsrf is true.
     */
    private function addCsrfField()
    {
        if ($this->createCsrf) {
            $this->fields[$this->name] = new HiddenField(array(
                'name' => $this->name,
                'value' => $this->csrf,
            ));
        }
        $this->setCreateCsrfOff();
    }

    /**
     * @return self
     */
    public function setCreateCsrfOff()
    {
        $this->createCsrf = false;
        return $this;
    }

    /**
     * @param $csrf
     * @return self
     */
    public function setCsrf($csrf)
    {
        $this->csrf = $csrf;
        return $this;
    }

    /**
     * @param $namePrefix
     * @return self
     */
    public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;
        return $this;
    }
}
