<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\DomElements\DomElement;
use \Library\DomElements\DomElementContainer;

class Form extends DomElementContainer
{
    private $template = '<form action="$$uri$$" method="$$method$$" $$enctype$$>$$children$$</form>';
    private $uri = '';
    private $method = 'POST';
    private $enctype = '';

    public function buildWidget()
    {
        $this->setTagName('form');
        $this->method();
        parent::buildWidget();
    }

    public function setUri($uri)
    {
        $this->addAttribute('action', $uri);
        return $this;
    }

    private function method()
    {
        $this->addAttribute('method', $this->method);
    }

    public function setMethodGet()
    {
        $this->method = 'GET';
        return $this;
    }

    /**
     * @override domelementcontainer::addchild
     * ignore child::createcontainer.
     */
    public function addChild(DomElement $child)
    {
        $this->children[] = $child->__tostring();
        return $this;
    }
}
