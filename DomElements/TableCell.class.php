<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\DomElements\DomElement;

class TableCell extends DomElement
{
    private $isForTableBody = true;
    private $content = '';
    private $cellTypes = array(
        'tableHead' => 'h',
        'tableBody' => 'd',
    );
    private $template = '<t$$cellTag$$ class="$$classes$$">$$content$$</t$$cellTag$$>';
    public function __construct($isForTableBody = true, $content = '')
    {
        $this->isForTableBody = $isForTableBody;
        $this->content = $content;
    }
    private function cellTag()
    {
        if ($this->isForTableBody) {
            $cellTag = $this->cellTypes['tableBody'];
        } else {
            $cellTag = $this->cellTypes['tableHead'];
        }
        return $cellTag;
    }
    public function buildWidget()
    {
        $this->finalRender = str_replace(
            array(
                '$$cellTag$$',
                '$$classes$$',
                '$$content$$',
            ),
            array(
                $this->cellTag(),
                $this->classesString(),
                $this->content,
            ),
            $this->template
        );
    }
}
