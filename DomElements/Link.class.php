<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\DomElements\DomElementContainer;

class Link extends DomElementContainer
{
    private $template = '<a$$attributes$$>$$children$$</a>';
    private $text = '';

    public function setUri($uri)
    {
        $this->addAttribute('href', $uri);
        return $this;
    }

    public function addText($text, $separator = '')
    {
        $this->text = $text;
        return parent::addText($text, $separator);
    }

    public function setTitle($title = '')
    {
        if ($title != '') {
            $this->addAttribute('title', $title);
        } else {
            $this->addAttribute('title', $this->text);
        }
        return $this;
    }

    public function setTargetNewWindow()
    {
        $this->addAttribute('target', '_blank');
        return $this;
    }

    public function setRelationship($name)
    {
        $this->addAttribute('rel', $name);
        return $this;
    }

    public function setUriAsText()
    {
        $this->addText($this->getAttribute('href'));
    }

    public function buildWidget()
    {
        if ($this->getAttribute('title') == '') {
            $this->addAttribute('title', $this->text);
        }
        $this->finalRender = str_replace(
            array(
                '$$attributes$$',
                '$$children$$',
            ),
            array(
                $this->attributesString(),
                $this->childrenString(),
            ),
            $this->template
        );
    }
}
