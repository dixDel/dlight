<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\Interfaces\IDomElement;

//@todo set ErrorManager
abstract class DomElement implements IDomElement
{
    private $classes = array();
    private $createContainer = true;
    private $attributes = array();
    private $tagName = '';
    protected $finalRender = '';

    public function __construct($tagName = '')
    {
        $this->tagName = $tagName;
    }

    abstract protected function buildWidget();

    protected function tagName()
    {
        if (!$this->tagName) {
            //$this->logError(__METHOD__.':'.__LINE__.': tagName is not set.');
        }
        return $this->tagName;
    }

    protected function setTagName($tagName)
    {
        $this->tagName = $tagName;
    }

    public function id()
    {
        return $this->getAttribute('id');
    }

    public function setId($id)
    {
        $this->addAttribute('id', preg_replace('/([^a-zA-Z0-9\-_])/i', '', $id));
        return $this;
    }

    public function classesString()
    {
        return implode(' ', $this->classes);
    }

    public function setClasses(array $classes)
    {
        foreach ($classes as $class) {
            $this->addClass($class);
        }
        return $this;
    }

    public function addClass($class)
    {
        $this->classes[] = $class;
        return $this;
    }

    public function hasClass($class)
    {
        $hasClass = false;
        if (array_search($class, $this->classes) !== false) {
            $hasClass = true;
        }
        return $hasClass;
    }

    public function setTitle($title)
    {
        $this->addAttribute('title', $title);
        return $this;
    }

    protected function convertNewLineToBrElement($string)
    {
        return preg_replace('/\n/', '<br/>', $string);
    }

    public function __tostring()
    {
        $this->buildWidget();
        return $this->finalRender;
    }

    /**
     * @deprecated
     * Alias for $this::setNoContainer.
     * @param bool $createContainer unused! Create container is the default behavior,
     * so calling this method is always for turning it off.
     */
    public function setCreateContainer($createContainer)
    {
        $this->setNoContainer();
    }

    public function setContainer()
    {
        $this->createContainer = true;
        return $this;
    }

    public function setNoContainer()
    {
        $this->createContainer = false;
        return $this;
    }

    /**
     * @return createContainer
     */
    public function createContainer()
    {
        return $this->createContainer;
    }

    protected function attributesString()
    {
        $attributes = array();
        if (!empty($this->classes)) {
            $this->addAttribute('class', implode(' ', $this->classes));
        }
        foreach ($this->attributes as $attributeName => $attributeValue) {
            $attributes[] = $attributeName.'="'.$attributeValue.'"';
        }
        return ($finalString = implode(' ', $attributes)) ? " $finalString" : "";
    }

    protected function getAttribute($attributeName)
    {
        $attributeValue = '';
        if ($this->isAttributeSet($attributeName)) {
            $attributeValue = $this->attributes[$attributeName];
        }
        return $attributeValue;
    }

    public function addAttribute($attributeName, $attributeValue)
    {
        $this->attributes[$attributeName] = $attributeValue;
        return $this;
    }

    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $attributeName => $attributeValue) {
            $this->addAttribute($attributeName, $attributeValue);
        }
        return $this;
    }

    private function isAttributeSet($attributeName)
    {
        return array_key_exists($attributeName, $this->attributes);
    }

    protected function removeAttribute($attributeName)
    {
        unset($this->attributes[$attributeName]);
        return $this;
    }
}
