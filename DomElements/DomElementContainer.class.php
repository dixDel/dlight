<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\DomElements\DomElement;

/**
 * Represents DomElements which can contain children elements.
 * Example: <p>...</p>, but not <img ... />
 */
class DomElementContainer extends DomElement
{
    private $template = '<$$tagName$$$$attributes$$>$$children$$</$$tagName$$>';
    protected $children = array();

    protected function buildWidget()
    {
        $this->finalRender = str_replace(
            array(
                '$$tagName$$',
                '$$attributes$$',
                '$$children$$',
            ),
            array(
                $this->tagName(),
                $this->attributesString(),
                $this->childrenString(),
            ),
            $this->template
        );
    }

    /**
     * @return children
     */
    public function children()
    {
        return $this->children;
    }

    public function childrenString()
    {
        return implode('', $this->children);
    }

    /**
     * @param array $children
     * @return self
     */
    public function setChildren(array $children)
    {
        foreach ($children as $child) {
            $this->addChild($child);
        }
        return $this;
    }

    /**
     * @todo bad solution because cannot chain DomElementContainer methods if $child is returned.
     * Build $child final HTML and
     * add it to DomElementContainer::children if DomElement::createContainer is true.
     * @param DomElement $child
     * @return DomElement DomElementContainer if $child::createContainer, else $child.
     */
    public function addChild(DomElement $child)
    {
        if ($child->createContainer()) {
            $this->children[] = $child->__tostring();
            return $this;
        } else {
            return $child;
        }
    }

    public function addText($text, $separator = '')
    {
        $this->children[] = $text.$separator;
        return $this;
    }

    public function addSpace()
    {
        return $this->addText(' ');
    }
}
