<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\DomElements\DomElement;

class Image extends DomElement
{
    private $template = '<img$$attributes$$/>';

    public function buildWidget()
    {
        $this->finalRender = str_replace(
            array(
                '$$attributes$$',
            ),
            array(
                $this->attributesString(),
            ),
            $this->template
        );
    }

    public function setSource($source)
    {
        $this->addAttribute('src', $source);
        return $this;
    }

    /**
     * @param $alternativeText
     * @return self
     */
    public function setAlternativeText($alternativeText)
    {
        $this->addAttribute('alt', $alternativeText);
        return $this;
    }

    /**
     * @param $width
     * @return self
     */
    public function setWidth($width)
    {
        $this->addAttribute('width', (int)$width);
        return $this;
    }

    /**
     * @param $height
     * @return self
     */
    public function setHeight($height)
    {
        $this->addAttribute('height', (int)$height);
        return $this;
    }
}
