<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \DateTimeZone;
use \DOMDocument;
use \Exception;
use \Library\Interfaces\IConfig;
use \Library\Interfaces\ICache;
use \Library\Utils\ArrayUtils;

/**
 * Description of Config
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Config implements IConfig
{
    private $defaultTimeZone    = '';
    private $rootConfiguration  = array();
    private $configLocationVars = array();
    private $cache              = null;
    protected $vars             = array();

    public function __construct($additionalLocation = '')
    {
        require('rootConfiguration.php');
        $this->rootConfiguration['libraryPath'] = $libraryPath;
        $this->rootConfiguration['rootPath'] = $rootPath;
        $this->rootConfiguration['mainConfigPath'] = $mainConfigPath;
        $this->defaultTimeZone = $timezone;
        array_push($this->configLocationVars, 'mainConfigPath', 'applicationConfigPath');
        if ($additionalLocation) {
            $this->addConfigLocation($additionalLocation);
        }
    }

    public function addConfigLocation($additionalLocation)
    {
        $this->configLocationVars[] = $additionalLocation;
        return $this;
    }

    public function vars()
    {
        return $this->vars;
    }

    /**
     * Returns the value of $var if it exists.
     * If not done yet, parses config files. General config first, then bundle
     * specific.
     * @param string $var
     * @return mixed string or null if var does not exist
     */
    public function get($var)
    {
        if (!$this->vars) {
            $this->setVars();
        }
        if (isset($this->vars[$var])) {
            return $this->vars[$var];
        }
        return null;
    }

    public function addVar($varName, $value)
    {
        $this->vars[$varName] = $value;
        return $this;
    }

    public function getInt($var)
    {
        return (int)$this->get($var);
    }

    public function getString($var)
    {
        return (string)$this->get($var);
    }

    public function getBool($var)
    {
        return (bool)$this->get($var);
    }

    private function setVars()
    {
        $cacheFilename = 'Config-'.__FUNCTION__;
        if (!$this->cache || !($this->vars = $this->cache->readArray($cacheFilename))) {
            $this->vars = $this->rootConfiguration;
            foreach ($this->configLocationVars as $configLocation) {
                $this->parseConfigFile($this->vars[$configLocation]);
            }
            if ($this->cache) {
                $this->cache->writeArray($cacheFilename, $this->vars);
            }
        }
    }

    private function parseConfigFile($configurationFolder)
    {
        foreach ($this->getConfigFiles($configurationFolder) as $file) {
            $this->parseXml($file);
        }
    }

    /**
     * Parse configuration folders for config[...].xml files.
     *
     * @access private
     * @return array paths to XML files found.
     */
    private function getConfigFiles($configurationFolder)
    {
        $xmlFiles = glob($configurationFolder.DIRECTORY_SEPARATOR.'config*.xml');
        if ($xmlFiles === false) {
            $xmlFiles = array();
        }
        return $xmlFiles;
    }

    protected function parseXml($filename)
    {
        if (file_exists($filename)) {
            $xml = new DOMDocument;
            $xml->load($filename);

            $elements = $xml->getElementsByTagName('define');

            foreach ($elements as $element) {
                $isRootPath = (bool)$element->getAttribute('rootPath');
                if ($isRootPath || (bool)$element->getAttribute('filesystemPath')) {
                    $value = str_replace('/', DIRECTORY_SEPARATOR, $element->getAttribute('value'));
                } else {
                    $value = $element->getAttribute('value');
                }
                if ($isRootPath) {
                    $value = $this->vars['rootPath'].$value;
                }
                $this->vars[$element->getAttribute('var')] = $value;
            }
        }
    }

    // @todo move to utility
    public function timezone()
    {
        try {
            $dateTimeZone = new DateTimeZone($this->getString('timezone'));
        } catch (Exception $exception) {
            //@todo log
            $dateTimeZone = new DateTimeZone($this->defaultTimeZone);
        }
        return $dateTimeZone;
    }
 
    /**
     * @param $cache
     * @return self
     */
    public function setCache(ICache $cache)
    {
        $this->cache = $cache;
        return $this;
    }
}
