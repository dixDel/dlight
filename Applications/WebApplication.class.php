<?php
namespace Library\Applications;

use \DOMDocument;
use \Library\Applications\Application;
use \Library\HTTPRequest;
use \Library\HTTPResponse;
use \Library\Route;
use \Library\Router;
use \RuntimeException;

abstract class WebApplication extends Application
{
    protected $httpRequest = null;
    protected $httpResponse = null;
    protected $page = null;
    protected $router = null;

    public function __construct()
    {
        parent::__construct();
        $this->httpRequest = $this->factory->get('Library/Interfaces/IInput');
        $this->httpResponse = $this->factory->get('Library/Interfaces/IOutput');
        $this->page = $this->factory->get('Library/Page');
        $this->router = $this->factory->get('Library/Interfaces/IRouter');
    }

    public function getController()
    {
        try {
            // Gets route matching URL.
            $matchedRoute = $this->router->getRoute($this->httpRequest->requestURI());
        } catch (RuntimeException $exception) {
            $this->errorManager()->logException($exception);
            if ($exception->getCode() == $this->router->errorCodeNoRoute()) {
                $this->errorManager()->logError(
                    __METHOD__.':'.__LINE__.': '.$exception->getMessage().'; redirecting to 404.'
                );
                // If no route matches, then the page does not exist.
                // @todo create default Language entity
                $this->httpResponse->redirect404($this->page->addVar('languageActive', $this->language()));
            }
        }
        // Adds variables from the URL to $_GET
        $_GET = array_merge($_GET, $matchedRoute->vars());
        // Instanciates the controller.
        $controllerClass = 'Application/Bundles/'.$matchedRoute->bundle().'/'.$matchedRoute->bundle().'Controller';
        $this->config()->addVar('controllerAction', $matchedRoute->action());
        $this->config()->addVar('controllerBundle', $matchedRoute->bundle());
        return $this->factory->get($controllerClass);
    }

    /**
     * @return httpRequest
     */
    public function httpRequest()
    {
        return $this->httpRequest;
    }

    /**
     * @return httpResponse
     */
    public function httpResponse()
    {
        return $this->httpResponse;
    }

    /**
     * @return page
     */
    public function page()
    {
        return $this->page;
    }

    /**
     * @return router
     */
    public function router()
    {
        return $this->router;
    }
}
