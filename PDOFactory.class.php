<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

use \PDO;
use \PDOException;

/**
 * Description of PDOFactory
 *
 * Implements the Factory design pattern to deliver PDO objects for several
 * Relational DataBase Management System (RDBMS).
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class PDOFactory
{
    static protected $messages = array();
    static protected $supportedServers = array(
        'mysql',
        'sqlsrv',
    );

    public static function getMessages()
    {
        return self::$messages;
    }

    public static function getConnection(
        $server,
        $host,
        $database,
        $username = '',
        $password = ''
    ) {
        $db = null;
        if (array_search($server, self::$supportedServers) !== false) {
            $method = 'get'.ucfirst($server).'Connection';
            try {
                $db = self::$method($host, $database, $username, $password);
                self::configure($db);
            } catch (PDOException $e) {
                self::$messages[] =
                    $e->getFile().':'.$e->getLine().':'.$e->getMessage();
                self::$messages[] = $e->getTraceAsString();
            }
        } else {
            self::$messages[] = "This database {$server} is not supported.";
        }
        return $db;
    }

    public static function getMysqlConnection($host, $database, $username, $password)
    {
        $db = new PDO('mysql:host='.$host.';dbname='.$database, $username, $password);
        return $db;
    }

    public static function getSqlsrvConnection($host, $database, $username = '', $password = '')
    {
        $db = new PDO('sqlsrv:Server='.$host.';Database='.$database, $username, $password);
        return $db;
    }

    protected static function configure(&$db)
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_PERSISTENT, false);
        $db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'set names utf8mb4');
    }
}
