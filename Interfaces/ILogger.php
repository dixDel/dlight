<?php
namespace Library\Interfaces;

use \Exception;

interface ILogger
{
    public function __construct($logPath);
    public function logServerInfos();
    public function logMessage($message);
    public function logError($message);
    public function logException(Exception $exception);
}
