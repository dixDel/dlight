<?php
namespace Library\Interfaces;

use \Library\Interfaces\ILogger;

interface ICache
{
    public function __construct(ILogger $logger, $cacheDirectoryPath = 'Cache', $duration = 0);
    public function write($filename, $content);
    public function writeArray($filename, array $array);
    public function writeObject($filename, $object);
    public function read($filename);
    public function readArray($filename);
    public function readObject($filename);
    public function delete($filename);
    public function clear($startPattern = '');
    public function inc($fileContent, $cachename = null);
    public function start($cachename);
    public function end();
}
