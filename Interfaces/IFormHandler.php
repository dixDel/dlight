<?php
namespace Library\Interfaces;

use \Library\Interfaces\IApplication;
use \Library\Entity;
use \Library\Utils\StringUtils;

interface IFormHandler
{
    public function __construct(IApplication $app, Entity $entity, StringUtils $stringUtils, $formName);
    public function process();
    public function entity();
    public function setEntity(Entity $entity);
    public function setFormName($formName);
    public function setDebugOn();
    public function setDebugOff();
    public function setEmbeddedFormName($embeddedFormName);
    public function setEmbeddedFormCount($embeddedFormCount);
    public function entities();
    public function embeddedFormCount();
}
