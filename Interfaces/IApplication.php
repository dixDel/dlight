<?php
namespace Library\Interfaces;

use \Library\Interfaces\ICache;
use \Library\Interfaces\IConfig;
use \Library\Interfaces\iErrorManager;
use \Library\Interfaces\IFactory;
use \Library\Interfaces\iLanguage;
use \Library\Interfaces\IUser;

interface IApplication
{
    public function __construct();
    public function run();
}
