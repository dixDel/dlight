<?php
namespace Library\Interfaces;

use \Library\Route;

interface IRouter
{
    public function addRoute(Route $route);
    public function getRoute($url);
    public function errorCodeNoRoute();
}
