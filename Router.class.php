<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Library\Interfaces\ICache;
use \Library\Interfaces\IRouter;
use \Library\FileManagers\XmlFileManager;
use \Library\Route;
use \RuntimeException;
use \SimpleXMLElement;

/**
 * Description of Router
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Router implements IRouter
{
    private $errorCodeNoRoute = 1;
    private $routes           = array();
    private $routesFilePath   = '';
    private $xmlFileManager   = null;
    private $cache            = null;

    public function __construct(XmlFileManager $xmlFileManager, $routesFilePath, ICache $cache = null)
    {
        $this->xmlFileManager = $xmlFileManager;
        $this->routesFilePath = $routesFilePath;
        $this->cache = $cache;
        $this->init();
    }

    private function init()
    {
        $cacheFilename = 'Router-routes';
        if (!$this->cache || !($this->routes = $this->cache->readArray($cacheFilename))) {
            $routes = $this->xmlFileManager->openXmlFile($this->routesFilePath);
            foreach ($routes as $route) {
                $routeAttributes = $route->attributes();
                $url = (string)$routeAttributes['url'];
                $bundle = (string)$routeAttributes['bundle'];
                $action = (string)$routeAttributes['action'];
                $vars = $this->getRouteVars($route);
                $route = new Route($url, $bundle, $action, $vars);
                $this->addRoute($route);
                if ($this->cache) {
                    $this->cache->writeArray($cacheFilename, $this->routes);
                }
            }
        }
    }

    private function getRouteVars(SimpleXMLElement $route)
    {
        $vars = array();
        $routeAttributes = $route->attributes();
        if (isset($routeAttributes['vars'])) {
            $vars = explode(',', $routeAttributes['vars']);
        }
        return $vars;
    }

    public function addRoute(Route $route)
    {
        if (!in_array($route, $this->routes)) {
            $this->routes[] = $route;
        }
    }

    public function getRoute($url)
    {
        foreach ($this->routes as $route) {
            // If the route matches the URL
            if (($varsValues = $route->match($url)) !== false) {
                // If the route has vars
                if ($route->hasVars()) {
                    $varsNames = $route->varsNames();
                    $listVars = array();
                    // $varsValues is an associative array with variable name as key, and variable value as value
                    foreach ($varsValues as $key => $match) {
                        // The first value contains the entire captured string (see doc for preg_match)
                        if ($key) {
                            $listVars[$varsNames[$key - 1]] = $match;
                        }
                    }
                    // Assigns this array to the route.
                    $route->setVars($listVars);
                }
                return $route;
            }
        }
        throw new RuntimeException("No route matches the URL $url", $this->errorCodeNoRoute);
    }

    /**
     * @return errorCodeNoRoute
     */
    public function errorCodeNoRoute()
    {
        return $this->errorCodeNoRoute;
    }
 
    /**
     * @param $cache
     * @return self
     */
    public function setCache(ICache $cache)
    {
        $this->cache = $cache;
        return $this;
    }
}
