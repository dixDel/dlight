<?php
namespace Library;

use \Library\ClassParameter;

class ClassDependency extends ClassParameter
{
    private $classNamespace = 'Library';

    /**
     * @return classNamespace
     */
    public function classNamespace()
    {
        return $this->classNamespace;
    }

    /**
     * @param $classNamespace
     * @return self
     */
    public function setClassNamespace($classNamespace)
    {
        $this->classNamespace = $classNamespace;
        return $this;
    }
}
