<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Library\Interfaces\IConfig;
use \Library\Interfaces\ILogger;
use \Library\Utils\ArrayUtils;
use PHPMailer;

/**
 * Extends PHPMailer with custom default settings and error manager.
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Mail extends PHPMailer
{
    private $config = null;
    private $errorManager = null;
    private $arrayUtils = null;
    private $listTo = array();
    private $listCc = array();
    private $listBcc = array();
    protected $errorEncountered = false;

    /**
     * Initialise mail server settings according to $config.
     * @param Config $config
     * @param ErrorManager $errorManager
     * @todo check Config
     * @todo refactor: use dependency injection for parameters
     */
    public function __construct(IConfig $config, ILogger $errorManager, ArrayUtils $arrayUtils)
    {
        parent::__construct();
        $this->config = $config;
        $this->errorManager = $errorManager;
        $this->arrayUtils = $arrayUtils;
        $this->Host = $config->getString('mailHost');
        if ($config->getBool('mailSMTPAuth')) {
            $this->isSMTP();
            $this->SMTPAuth = true;
            $this->SMTPSecure = $config->getString('mailSMTPSecure');
        }
        $this->Username = $this->config->getString('mailDefaultUsername');
        $this->Password = $this->config->getString('mailDefaultPassword');
        $this->CharSet = 'UTF-8';
    }

    private function initMailAccount()
    {
        if (!$this->config->getBool('production')) {
            $this->errorManager->logMessage(
                __METHOD__.':'.__LINE__.
                ': production is set to false; using webmaster account.'
            );
            $this->Username = $this->config->getString('webmasterAddress');
            $this->Password = $this->config->getString('mailPassword');
            // From must match sender to avoid SMTP Error: data not accepted.
            $this->From = $this->Username;
        }
        $this->errorManager->logMessage(
            __METHOD__.':'.__LINE__.': using account '.$this->Username.
            ' (password '.($this->Password ? 'set' : 'NOT set').').'
        );
    }

    public function send()
    {
        if (!$this->errorEncountered) {
            $this->initMailAccount();
            $this->errorManager->logMessage(
                __METHOD__.':'.__LINE__.': direct recipients are '.
                implode(', ', $this->listTo).'.'
            );
            $this->errorManager->logMessage(
                __METHOD__.':'.__LINE__.': carbon copy recipients are '
                .implode(', ', $this->listCc).'.'
            );
            $this->errorManager->logMessage(
                __METHOD__.':'.__LINE__.': hidden carbon copy recipients are '.
                implode(', ', $this->listBcc).'.'
            );
            if (!$this->config->getBool('production')) {
                $debugAddress = $this->config->getString('webmasterAddress');
                $this->errorManager->logMessage(
                    __METHOD__.':'.__LINE__.": production is set to false;".
                    " clearing all recipients and sending to $debugAddress only."
                );
                $this->clearAllRecipients();
                $this->addAddress($debugAddress);
            }
            if ($this->errorEncountered = !parent::send()) {
                $this->errorManager->logError(__METHOD__.':'.__LINE__.': '.$this->ErrorInfo);
                $this->errorManager->logError(
                    __METHOD__.':'.__LINE__.": Host={$this->Host}, ".
                    "SMTPAuth={$this->SMTPAuth}, SMTPSecure={$this->SMTPSecure}, ".
                    "Username={$this->Username}, Password={$this->password()}, ".
                    "CharSet={$this->CharSet}, From={$this->From}, ".
                    "FromName={$this->FromName}, WordWrap={$this->WordWrap}, ".
                    "Subject={$this->Subject}, Body={$this->Body}."
                );
            }
        }
        return !$this->errorEncountered;
    }

    private function password()
    {
        $passwordStub = '*Password %sset*';
        if ($this->Password) {
            $passwordStub = sprintf($passwordStub, '');
        } else {
            $passwordStub = sprintf($passwordStub, 'not ');
        }
        return $passwordStub;
    }

    public function errorEncountered()
    {
        return $this->errorEncountered;
    }

    /**
     * @return config
     */
    protected function config()
    {
        return $this->config;
    }

    /**
     * @return errorManager
     */
    protected function errorManager()
    {
        return $this->errorManager;
    }

    /**
     * Override PHPMailer::addAnAddress
     * @param string $kind (to, cc, bcc...)
     * @param string $address A valid email address.
     * @param string $name optional display name.
     */
    protected function addAnAddress($kind, $address, $name = '')
    {
        parent::addAnAddress($kind, $address, $name);
        $arrayName = 'list'.ucfirst(strtolower($kind));
        // PHPMailer uses more kinds that this class, like Reply-to.
        if (isset($this->$arrayName)) {
            array_push($this->$arrayName, $address);
        }
    }

    /**
     * Override PHPMailer::clearAllRecipients
     */
    public function clearAllRecipients()
    {
        parent::clearAllRecipients();
        $this->listTo = array();
        $this->listCc = array();
        $this->listBcc = array();
    }
}
