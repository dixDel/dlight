<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Utils;

use \Library\Interfaces\IStringUtils;

class StringUtils implements IStringUtils
{
    /**
     * Convert specified value from UTF-8 to Windows-1252
     * @todo let user choose encodage (but limit to 2 args when method used with array_walk)
     * @param <type> $value
     * @param <type> $key
     * @return null
     */
    public function excelEncode(&$value, $key)
    {
        $value = iconv('UTF-8', 'Windows-1252', $value);
        return null;
    }

    public function multiExplode($pattern, $string, $standardDelimiter = ' ')
    {
        // replace delimiters with standard delimiter, also removing redundant delimiters
        $string = preg_replace(array($pattern, "/{$standardDelimiter}+/s"), $standardDelimiter, $string);
        // return the results of explode
        return explode($standardDelimiter, $string);
    }

    /**
     * Replaces special chars of $str by simple ASCII equivalent
     * and removes other special chars which could create problems in filename.
     * @param string $str the string to clean
     * @return string the cleaned string
     */
    public function reallyCleanString($str)
    {
        $str = mb_strtolower(trim($str), 'UTF-8');
        $str = $this->strtrUTF8(
            $str,
            "ÀÁÂÃÄÅàáâãäåÇçÒÓÔÕÖØòóôõöøÈÉÊËèéêëÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ,",
            "AAAAAAaaaaaaCcOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNn_"
        );
        $str = preg_replace('/[^a-z0-9\-\.,\*]/', '-', $str);
        $str = preg_replace_callback(
            '/([\-\.,\*]{2,})/u',
            function ($matches) {
                return substr($matches[0], 0, 1);
            },
            $str
        );
        $str = preg_replace('/^[^a-z0-9]|[^a-z0-9]$/', '', $str);
        $str = str_replace('.', '-', $str);
        return ($str);
    }

    /**
     * UTF-8 compatible strtr.
     * http://stackoverflow.com/questions/1454401/how-do-i-do-a-strtr-on-utf-8-in-php
     * @param string $str
     * @param string $from
     * @param string $to
     * @return string
     */
    public function strtrUTF8($str, $from, $to)
    {
        $keys = array();
        $values = array();
        preg_match_all('/./u', $from, $keys);
        preg_match_all('/./u', $to, $values);
        $mapping = array_combine($keys[0], $values[0]);
        return strtr($str, $mapping);
    }

    /**
     * Strip $phoneNumber of non digits, except + and () if used as prefix,
     * and add a space separator every three digits.
     *
     * @param string $phoneNumber
     * @return string The processed string
     */
    public function nicePhoneNumber($phoneNumber)
    {
        $nicePhoneNumber = '';
        $phoneNumber = trim($phoneNumber);
        if (strpos($phoneNumber, '(') === 0) {
            $closingParenthesisPosition = strpos($phoneNumber, ')');
            $nicePhoneNumber = substr($phoneNumber, 1, $closingParenthesisPosition - 1).' ';
            $phoneNumber = substr($phoneNumber, $closingParenthesisPosition + 1);
        } elseif (strpos($phoneNumber, '+') === 0) {
            $nicePhoneNumber = '+';
        }
        $phoneNumber = preg_replace('/[^\d\(\)]*/', '', $phoneNumber);
        if ($modulo = (strlen($phoneNumber) % 3)) {
            $nicePhoneNumber .= substr($phoneNumber, 0, $modulo);
            $phoneNumber = substr($phoneNumber, $modulo);
        }
        if (strlen($nicePhoneNumber) > 1) {
            $nicePhoneNumber .= ' ';
        }
        $nicePhoneNumber .= implode(' ', str_split($phoneNumber, 3));
        return $nicePhoneNumber;
    }

    private $pluralToSingularList = array(
        'ies' => 'y',
        's' => '',
    );
    /**
     * Put $word in singular form.
     * @param string $word.
     * @return string The processed word.
     */
    public function strtosingular($word)
    {
        $wordLength = strlen($word);
        //Goddess::debugVar(strrpos($word, 'ies'), true);
        foreach (self::$pluralToSingularList as $plural => $singular) {
            if (($positionIndex = strrpos($word, $plural)) === $wordLength - strlen($plural)) {
                $word = substr($word, 0, $positionIndex).$singular;
            }
        }
        return $word;
    }

    /**
     * Fill in specified string by specified values according to their order,
     * using pattern or default one if not specified.
     * @example strMultiReplace('Here is %v1 and %v2. But %v1 is repeated.', array('val1', 'val2'));<br/>
     * returns: Here is val1 and val2. But val1 is repeated.
     * @param string $string
     * @param array $valuesToInsert
     * @param string $patternGeneric Optional
     * @return string
     */
    public function strMultiReplace($string, array $valuesToInsert, $patternGeneric = '%v')
    {
        $completedString = $string;
        $count = 1;
        foreach ($valuesToInsert as $value) {
            $pattern = $patternGeneric . $count;
            $completedString = str_replace($pattern, $value, $completedString);
            $count++;
        }
        return $completedString;
    }

    /**
     * Fill in specified string by specified values according to their order,
     * using pattern or default one if not specified.
     * @example completeString('Here is $v1 and $v2. But $v1 is repeated.', array('val1', 'val2'));<br/>
     * returns: Here is val1 and val2. But val1 is repeated.
     * @param string $string
     * @param array $valuesToInsert
     * @param string $patternGeneric Optional
     * @return string
     */
    public function completeString($string, array $valuesToInsert, $patternGeneric = '%v')
    {
        $completedString = $string;
        $count = 1;
        foreach ($valuesToInsert as $value) {
            $pattern = $patternGeneric . $count;
            $completedString = str_replace($pattern, $value, $completedString);
            $count++;
        }
        return $completedString;
    }

    public function className($object)
    {
        $className = get_class($object);
        if (($lastSeparatorPosition = strrpos($className, '\\')) !== false) {
            $className = substr($className, $lastSeparatorPosition + 1);
        }
        return $className;
    }

    public function slashesToAntislashes($string)
    {
        return str_replace('/', '\\', $string);
    }

    public function explodeFullClassName($fullClassName, $separator = '/')
    {
        $lastSeparatorPosition = strrpos($fullClassName, $separator);
        $namespace = substr($fullClassName, 0, $lastSeparatorPosition);
        $class = substr($fullClassName, $lastSeparatorPosition + 1);
        return array($namespace, $class);
    }

    public function addNamespace($className, $namespace)
    {
        return "$namespace/$className";
    }

    public function prefixFullClassNameWithAntiSlash($fullClassName)
    {
        if (strpos($fullClassName, '\\') !== 0) {
            $fullClassName = '\\'.$fullClassName;
        }
        return $fullClassName;
    }
}
