<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Utils;

class StringHtmlUtils
{
    public function convertTagContentToHtmlentities($text, $tagName)
    {
        $convertedText = '';
        $startTag = "<$tagName>";
        $endTag = "</$tagName>";
        while (($positionStart = strpos($text, $startTag)) !== false) {
            /*
             * //after 1 loop, $text starts with </pre>
             * so we ignore it.
             */
            $positionEnd = strpos($text, $endTag, 1);
            $positionStart += strlen($startTag);
            $convertedText .= substr($text, 0, $positionStart);
            $convertedText .= htmlentities(substr($text, $positionStart, $positionEnd - $positionStart));
            $text = substr($text, $positionEnd);
        }
        $convertedText .= $text;
        return $convertedText;
    }

    private function stripTags($html, $tagName)
    {
        $startTag = "<$tagName>";
        $endTag = "</$tagName>";
        $positionStart = strpos($html, $startTag);
        $positionEnd = strpos($html, $endTag);
        return substr($html, $positionStart + strlen($startTag), $positionEnd - strlen($startTag));
    }

    private function deleteIncompleteTags($text)
    {
        return preg_replace('/<[^>]*$/', '', $text);
    }

    public function closeTags($text)
    {
        // Delete incomplete opening and closing tags, including auto-closing ones.
        $newtext = $this->deleteIncompleteTags($text);
        if (substr($newtext, 0, 1) !== '<') {
            $newtext = "<p>".$newtext;
        }
        // match all opening and closing tags
        // (?:...) tells regex to not capture the subpattern (override default behavior)
        // @source http://nl3.php.net/manual/en/regexp.reference.subpatterns.php
        preg_match_all('/<\/?[a-z]+(?: [a-z0-9]+="[^<>"]*")*>/', $newtext, $matches);
        $unclosedTags = array();
        foreach ($matches[0] as $key => $tag) {
            $tag = preg_replace('/</', '', $tag);
            $tag = preg_replace('/(>| .*)$/', '', $tag);
            $firstChar = substr($tag, 0, 1);
            if ($firstChar !== '/') {
                $unclosedTags[] = $tag;
            } else {
                $tag = substr($tag, 1);
                if (($key = array_search($tag, $unclosedTags)) !== false) {
                    unset($unclosedTags[$key]);
                }
            }
        }
        $unclosedTags = array_reverse($unclosedTags);
        foreach ($unclosedTags as $key => $tag) {
            // Delete <a> tags rather than close them
            // because it makes no sense to leave a partial link.
            if ($tag == 'a') {
                $newtext = substr($newtext, 0, strrpos($newtext, '<a'));
            } else {
                // Delete the tag if it is empty (no text after the opening tag).
                $openingTag = "<$tag>";
                if (($openingTagPosition = strrpos($newtext, $openingTag)) < strlen($newtext) - strlen($openingTag)) {
                    $newtext .= "</$tag>";
                } else {
                    $newtext = substr($newtext, 0, $openingTagPosition);
                }
            }
        }
        return $newtext;
    }

    public function replacePlaceholdersWithHtmlTags($text, array $htmlTags)
    {
        $text = $this->deleteIncompleteTags($text);
        $placeholders = array();
        $tags = array();
        foreach ($htmlTags as $htmlTag) {
            $placeholders[] = $this->generatePlaceholder($htmlTag->position());
            $tags[] = $htmlTag->tag();
        }
        return str_replace($placeholders, $tags, $text);
    }

    public function generatePlaceholder($number)
    {
        return '<$'.sprintf('%02s', $number).'$>';
    }
}
