<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Utils;

class Utils
{
    public function defaultValue($attributeType)
    {
        switch($attributeType)
        {
            case 'string':
                $attributeValue = '';
                break;
            case 'array':
                $attributeValue = array();
                break;
            default:
                $attributeName = null;
                break;
        }
        return $attributeValue;
    }

    /**
     * Compares $object1 with $object2, using $keys, and returns true if they are identical.
     * @param Object $object1
     * @param Object $object2
     * @param array $keys
     * @param bool $strict indicates to compare types as well, true by default
     * @return bool true if $object1 equals $object2
     */
    public function areObjectsIdentical($object1, $object2, array $keys, $strict = true)
    {
        $identical = true;
        foreach ($keys as $key) {
            if (!isset($object1[$key]) || !isset($object2[$key])) {
                $identical = false;
            } else {
                if ($strict) {
                    if ($object1[$key] !== $object2[$key]) {
                        $identical = false;
                    }
                } else {
                    if ($object1[$key] != $object2[$key]) {
                        $identical = false;
                    }
                }
            }
            if (!$identical) {
                break;
            }
        }
        return $identical;
    }

    public function base64EncodeImage($filename = string, $filetype = string)
    {
        if ($filename) {
            $imgbinary = fread(fopen($filename, "r"), filesize($filename));
            return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
        }
    }
}
