<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Utils;

use \DateTime;
use \DateTimeZone;

/**
 * Tools for \DateTime objects.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class DateUtils
{
    private $timezone = null;

    public function __construct(DateTimeZone $timezone)
    {
        $this->timezone = $timezone;
    }

    public function isWorkingDay(DateTime $dateToTest, array $holidays, $includeWeekend = true)
    {
        return !$this->isNonWorkingDay($dateToTest, $holidays, $includeWeekend);
    }

    public function isNonWorkingDay(DateTime $dateToTest, array $holidays, $includeWeekend = true)
    {
        $isNonWorkingDay = false;
        if ($includeWeekend) {
            $isNonWorkingDay = $this->isWeekend($dateToTest);
        }
        if (!$isNonWorkingDay) {
            $isNonWorkingDay = in_array($dateToTest, $holidays);
        }
        return $isNonWorkingDay;
    }

    /**
     * Return true if week day number is 0 or 6 (Sunday or Saturday).
     * @param DateTime $dateToTest
     * @return bool
     */
    public function isWeekend(DateTime $dateToTest)
    {
        return array_search($this->weekDayNumber($dateToTest), array(0, 6)) !== false;
    }

    public function weekDayNumber(DateTime $date)
    {
        return (int)$date->format('w');
    }

    /**
     * @param string $date 'Ymd'
     */
    public function createDate($date, $format = 'Ymd')
    {
        return DateTime::createFromFormat($format, $date, $this->timezone)->setTime(0, 0, 0);
    }
}
