<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Utils;

class ArrayUtils
{
    public function sortArray($array)
    {
        sort($array);
        return $array;
    }

    /**
     * Similar to array_keys, but returns keys whose associated value contains part of a string.
     * Currently case insensitive.
     * @link http://nl1.php.net/manual/en/function.array-keys.php description
     */
    public function arrayKeysPartial($input, $search_value)
    {
        $keys = array();
        foreach ($input as $key => $value) {
            if (stripos($value, $search_value) !== false) {
                $keys[] = $key;
            }
        }
        return $keys;
    }

    /**
     * Join array elements recursively with a string.
     * @param string $glue
     * @param array $array
     * @param string $glueForSubarray = PHP_EOL
     * @param bool $trim = true. Trim all strings.
     * @return string containing a string representation of all the array elements in the same order,
     * with the glue string between each element.
     * @source http://stackoverflow.com/questions/3899971/implode-and-explode-multi-dimensional-arrays
     */
    public function recursiveImplode($glue, array $array, $glueForSubarray = PHP_EOL, $trim = true)
    {
        $result = '';
        //while (check if child is array)
        foreach ($array as $item) {
            if (is_array($item)) {
                $result .= substr(
                    $glueForSubarray.self::recursiveImplode($glue, $item, $glueForSubarray),
                    0,
                    -strlen($glue)
                );
            } else {
                if ($trim) {
                    $item = trim($item);
                }
                $result .= $item.$glue;
            }
        }
        return $result;
    }

    /**
     * Searches a multidimensionnal array for a given value and returns the corresponding key if successful.
     * @param array $haystack
     * @param mixed $needle
     * @param <type> $index
     * @return mixed false if no match
     */
    public function recursiveArraySearch($haystack, $needle, $index = null)
    {
        $aIt = new \RecursiveArrayIterator($haystack);
        $it = new \RecursiveIteratorIterator($aIt);
        while ($it->valid()) {
            if (((isset($index) && ($it->key() == $index)) || (!isset($index))) && ($it->current() == $needle)) {
                return $aIt->key();
            }
            $it->next();
        }
        return false;
    }

    /**
     * Return an associative array created from $baseArray, using $keyAsKey and $keyAsValue.
     * If $filterKey is set and $filterValue is matched, the entry will not be created in $finalArray.
     * @param array $baseArray an array of keys=>arrays.
     * @param string $keyAsKey the name of the key in $baseArray to use as key in the returned array.
     * @param string $keyAsValue the name of the key in $baseArray to use as value in the returned array.
     * @param string $filterKey optional.
     * @param string $filterValue optional.
     * @return array The array generated. It is empty if no key match $keyAsKey or $keyAsValue.
     */
    public function createAssociativeArray(array $baseArray, $keyAsKey, $keyAsValue, $filterKey = '', $filterValue = '')
    {
        $finalArray = array();
        foreach ($baseArray as $key => $array) {
            if (array_key_exists($keyAsKey, $array) && array_key_exists($keyAsValue, $array)) {
                if ($filterKey == '' || (array_key_exists($filterKey, $array) && $array[$filterKey] != $filterValue)) {
                    $finalArray[$array[$keyAsKey]] = $array[$keyAsValue];
                }
            }
        }
        return $finalArray;
    }

    /**
     * Similar to array_keys, but returns keys which contain part of a string.
     * @link http://nl1.php.net/manual/en/function.array-keys.php description
     */
    public function arrayKeysContain($input, $search_value, $strict = false)
    {
        $tmpkeys = array();

        $keys = array_keys($input);
        foreach ($keys as $k) {
            if ($strict && strpos($k, $search_value) !== false) {
                $tmpkeys[] = $k;
            } elseif (!$strict && stripos($k, $search_value) !== false) {
                $tmpkeys[] = $k;
            }
        }

        return $tmpkeys;
    }

    /**
     * Returns an array of int from $key's value of each array in $arrays.
     * @param array $arrays
     * @param string $key
     * @return array
     */
    public function intFromSubarray(array $arrays, $key)
    {
        $intArray = array();
        foreach ($arrays as $array) {
            $intArray[] = (int)$array[$key];
        }
        return $intArray;
    }

    public function checkIdenticalSize(array $first, array $second)
    {
        if (count($first) != count($second)) {
            throw new Exception('The two arrays must have the same size.');
        }
    }

    public function addEmptyArrayToChoicesList(array &$choices)
    {
        $choices = array(array(0 => '', true))+$choices;
    }

    public function addEmptyValueToChoicesList(array &$choices)
    {
        $choices = array(0 => '')+$choices;
    }

    /**
     * Returns the value of $attributeName in the superglobal $_SERVER.
     * @param string $attributeName
     * @return string The corresponding value or an empty string.
     */
    public function serverData($attributeName)
    {
        $value = '';
        if (isset($_SERVER[$attributeName])) {
            $value = $_SERVER[$attributeName];
        }
        return $value;
    }
}
