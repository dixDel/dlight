<?php
namespace Library\Fakes;

use \Library\Interfaces\ICache;
use \Library\Interfaces\ILogger;

class FakeCache implements ICache
{
    public function __construct(ILogger $logger, $cacheDirectoryPath = 'Cache', $duration = 0)
    {
    }

    public function write($filename, $content)
    {
    }

    public function writeArray($filename, array $array)
    {
    }

    public function writeObject($filename, $object)
    {
    }

    public function read($filename)
    {
        return false;
    }
    public function readArray($filename)
    {
        return array();
    }
    public function readObject($filename)
    {
        return false;
    }
    public function delete($filename)
    {
    }

    public function clear($startPattern = '')
    {
    }

    public function inc($fileContent, $cachename = null)
    {
    }

    public function start($cachename)
    {
    }

    public function end()
    {
    }
}
