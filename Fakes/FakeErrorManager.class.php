<?php
namespace Library\Fakes;

use \Exception;
use \Library\Interfaces\ILogger;

class FakeErrorManager implements ILogger
{
    public function __construct($logPath)
    {
    }

    public function logServerInfos()
    {
    }

    public function logMessage($message)
    {
    }

    public function logError($message)
    {
    }

    public function logException(Exception $exception)
    {
    }
}
