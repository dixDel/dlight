<?php
namespace Library\Fakes;

use \Library\Applications\Application;
use \Library\Factory;

/**
 * Fake implementation of IApplication.
 **/
class FakeApplication extends Application
{
    public function __construct()
    {
        $this->setFactory(new Factory);
        $this->init();
        //$this->cache = $this->factory->getFake('Cache');
        $this->config = $this->factory->getFake('Config');
        $this->errorManager = $this->factory->get('Library/Interfaces/ILogger');
        //$this->user = $this->factory->getFake('User');
    }

    protected function init()
    {
        $this->name = 'FakeApplication';
        $this->factory->addObject($this);
    }

    public function run()
    {
    }
}
